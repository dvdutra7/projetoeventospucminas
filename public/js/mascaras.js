var somenteNumeros = { 
	campos : '[name="cpf"], [name="cep"], [name="nascimento"], [name="cnpj"], [name="cep_consulta"]',
	opcoes: { title:'Somente Números', placement:'top', trigger:'focus'},
};

function maskFields(){
	// Mobile e navegadores antigos
	if($('html').hasClass('touch')){

		// CPF
		$("input[name=cpf]").keyup(function(event){
			var val = $(this).val()
			,	digit = val.slice(-1)
			,	cpfRegex = /(\d{3})(.\b)(\d{3})(.\b)(\d{3})(-\b)(\d{2})/g;

			if(digit.replace(/[^\d]/g, '') != '' || (digit == '-' || digit == '.') ){
				if(event.which != 8){
					if(val.length == 3){
						$(this).val(val+'.');
					}
					else if (val.length == 7){
						$(this).val(val+'.');
					}
					else if (val.length == 11){				
						$(this).val(val+'-');
					}
				}
			}else{
				$(this).val(val.slice(0,-1))
			}
			if(val.length == 14 && cpfRegex.test(val)){
				if(validaCPF(val)){
					//$('.tooltip').addClass('tooltipOk').find('.tooltip-inner').text('Ok');
					$('.tooltip').find('.tooltip-inner').text('Ok');
				} 
			}else{
				//$('.tooltip').removeClass('tooltipOk').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
				$('.tooltip').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
			}
		}).focusin(function(){
			$(this).attr({maxlength: '14', type:'tel'});
		});

		// NASCIMENTO 
		$("input[name=nascimento]").keyup(function(event){
			var val = $(this).val()
			,	digit = val.slice(-1)
			,	nascimentoRegex = /(^\d{2})\/(\d{2})\/(\d{4}$)/g;

			if(digit.replace(/[^\d]/g, '') != '' || digit == '/'){
				if(event.which != 8){
					if(val.length == 2){
						$(this).val(val+'/');
					}
					else if (val.length == 5){
						$(this).val(val+'/');
					}
				}
			}else{
				$(this).val(val.slice(0,-1))
			}
			if(val.length == 10 && nascimentoRegex.test(val)){
				//$('.tooltip').addClass('tooltipOk').find('.tooltip-inner').text('Ok');
				$('.tooltip').find('.tooltip-inner').text('Ok');
			}else{
				//$('.tooltip').removeClass('tooltipOk').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
				$('.tooltip').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
			}
		}).focusin(function(){
			$(this).attr({maxlength: '10', type:'tel'});
		});

		// CEP
		$('input[name="cep"]').keyup(function(event) {
			var val = $(this).val()
			,	digit = val.slice(-1)
			,	cepRegex = /(^\d{5})(-\b)(\d{2})/g;

			if(digit.replace(/[^\d]/g, '') != '' || digit == '-'){
				if(event.which != 8){
					if(val.length == 5){
						$(this).val(val+'-');
					}
				}
			}else{
				$(this).val(val.slice(0,-1))
			}
			if(val.length == 9 && cepRegex.test(val)){
				//$('.tooltip').addClass('tooltipOk').find('.tooltip-inner').text('Ok');
				$('.tooltip').find('.tooltip-inner').text('Ok');
			}else{
				//$('.tooltip').removeClass('tooltipOk').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
				$('.tooltip').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
			}
		}).focusin(function(){
			$(this).attr({maxlength: '9', type:'tel'});
		});

		// CEP
		$('input[name="cep_consulta"]').keyup(function(event) {
			var val = $(this).val()
			,	digit = val.slice(-1)
			,	cepRegex = /(^\d{5})(-\b)(\d{2})/g;

			if(digit.replace(/[^\d]/g, '') != '' || digit == '-'){
				if(event.which != 8){
					if(val.length == 5){
						$(this).val(val+'-');
					}
				}
			}else{
				$(this).val(val.slice(0,-1))
			}
			if(val.length == 9 && cepRegex.test(val)){
				//$('.tooltip').addClass('tooltipOk').find('.tooltip-inner').text('Ok');
				$('.tooltip').find('.tooltip-inner').text('Ok');
			}else{
				//$('.tooltip').removeClass('tooltipOk').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
				$('.tooltip').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
			}
		}).focusin(function(){
			$(this).attr({maxlength: '9', type:'tel'});
		});

		// CNPJ
		$('input[name="cnpj"]').keyup(function(event) {
			var val = $(this).val()
			,	digit = val.slice(-1)
			,	cepRegex = /(^\d{2})(.\b)(\d{3})(.\b)(\d{3})\/(\d{4})(-\b)(\d{2}$)/g;

			if(digit.replace(/[^\d]/g, '') != '' || (digit == '-' || digit == '.' || digit == '/')){
				if(event.which != 8){
					if(val.length == 2){
						$(this).val(val+'.');
					}
					else if (val.length == 6){
						$(this).val(val+'.');
					}
					else if (val.length == 10){				
						$(this).val(val+'/');
					}
					else if (val.length == 15){				
						$(this).val(val+'-');
					}
				}
			}else{
				$(this).val(val.slice(0,-1))
			}
			if(val.length == 18 && cepRegex.test(val)){
				if(validaCNPJ(val)) 
					//$('.tooltip').addClass('tooltipOk').find('.tooltip-inner').text('Ok');
					$('.tooltip').find('.tooltip-inner').text('Ok');
			}else{
				//$('.tooltip').removeClass('tooltipOk').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
				$('.tooltip').find('.tooltip-inner').text(somenteNumeros.opcoes.title);
			}
		}).focusin(function(){
			$(this).attr({maxlength: '18', type:'tel'});
		});
		
		// tooltips
		$(somenteNumeros.campos).tooltip(somenteNumeros.opcoes);

	}else{
		$("input[name=nascimento]").mask("99/99/9999");
		$("input[name=cpf]").mask("999.999.999-99");
		$("input[name=cnpj]").mask("99.999.999/9999-99");
		$("input[name=cep]").mask("99999-999");
		$("input[name=cep_consulta]").mask("99999-999");
	}
}
jQuery(function($){
	if($('html').hasClass('touch')){
		// Transformar os dados no formato da mascara
		var cepVal
		,	cepMask;
		if($('input[name="cep"]').length > 0 && $('input[name="cep"]').val() != ""){
			cepVal = $('input[name="cep"]').val();
			cepMask = cepVal.slice(0,5) + "-" + cepVal.slice(-3);
			$('input[name="cep"]').val(cepMask);
		}
		var cep_consultaVal
		,	cep_consultaMask;
		if($('input[name="cep_consulta"]').length > 0 && $('input[name="cep_consulta"]').val() != ""){
			cep_consultaVal = $('input[name="cep_consulta"]').val();
			cep_consultaMask = cep_consultaVal.slice(0,5) + "-" + cep_consultaVal.slice(-3);
			$('input[name="cep_consulta"]').val(cep_consultaMask);
		}
		var cpfVal
		,	cpfMask;
		if($('input[name="cpf"]').length > 0 && $('input[name="cpf"]').val() != ""){
			cpfVal = $('input[name="cpf"]').val();
			cpfMask = cpfVal.slice(0,3) + "." + cpfVal.slice(3).slice(0,3) + "." + cpfVal.slice(6).slice(0,3)+"-"+ cpfVal.slice(9).slice(0,2);
			$('input[name="cpf"]').val(cpfMask);
		}
	}
	maskFields();	
});
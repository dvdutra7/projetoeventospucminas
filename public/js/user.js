$(document).on('submit', '#form-login', function(){

    var dados = $(this).serialize();

    if($('#form-login').valid()){
        loading('show');
        $.ajax({
            method: 'POST',
            url: '/login',
            data: {dados: dados},
            success: function(data){
                
                data = JSON.parse(data);
                if(data.auth == true){
                    location.replace(data.path);
                } else {
                    loading('hidden');
                    alert(data.mensagem);
                }
            },
            error: function(){
                loading('hidden');
                erroPadrao();
            }
        });
    }

    return false;
});

function esqueciMinhaSenha(){
    var elEmail = $('input[name=email]');
    elEmail.valid();
    $('#email-error').html('Digite seu e-mail aqui');
    if(!elEmail.val()){
        Swal.fire(
            'Atenção',
            'Digite seu e-mail',
            'warning'
        );
        return false;
    };
    Swal.fire({
        title: 'Atenção',
        text: "Enviaremos um e-mail para "+elEmail.val()+".",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Enviar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            loading('show');
            var email = elEmail.val();
            $.ajax({
                method: 'POST',
                url: '/recuperar-senha',
                data: { email },
                success: function (data) {
                    data = JSON.parse(data);
                    loading('hidden');
                    Swal.fire(
                        '',
                        data.mensagem,
                        data.type
                    );
                },
                error: function () {
                    loading('hidden');
                    erroPadrao();

                }
            });
        }
    })
}
function loading(action){
	if(action == 'show'){
		var div_loading = '<div id="loading">'
		+'<div id="bg-loading">'
		+'<div id="img-loading"><img src="/img/loading.gif" alt="Carregando" title="Carregando">'
		+'<div><span style="color: #337ab7"><b>Carregando</b></span></div></div>';
		+'</div></div>';
		$('body').append(div_loading);
	}else if(action == 'hidden'){
		$('#loading').remove();
	}
}

function erroPadrao(){
	loading('hidden');
	Swal.fire("", "Não foi possível efetuar esta operação, tente novamente em instantes.", "error");
}

function formPadrao(idForm){
    var dados = $(idForm).serialize();
    var action = $(idForm).attr('action');

    loading('show');

    if ($(idForm).valid()) {
        $.ajax({
            method: 'POST',
            url: action,
            data: { dados: dados },
            success: function (data) {
                loading('hidden');
                
                if(data.sucesso){
                    Swal.fire('', data.mensagem, 'success').then(() => {
                        if(typeof data.path !== 'undefined' && data.path){
                            location.replace(data.path);
                        }else{
                            document.location.reload(true);
                        }
                    });
                }else{
                    erroPadrao();
                }
            },
            error: function () {
                erroPadrao();
            },
            dataType:'json',
            async:false
        });
    }

    loading('hidden');

    return false;
}

function limpaCampo(e) {
    $('[name="pesquisar"]').val('');
}

function deslizar(alvo, altura) {
    if(altura == null)
        altura = 100;
    $('html, body').stop().animate({
        'scrollTop': alvo.offset().top + altura +'px'
    }, 900, 'swing');
}

function montaUrlFiltros(dados) {
    var url = '';
    var prefixo = '?';
    for (key in dados) {
        if (dados[key] != '' && dados[key] != undefined) {
            var value = dados[key];
            if (typeof value === 'string') {
                value = formataString(value);
            }
            url += prefixo + key + '=' + value;
            prefixo = '&';
        }
    }

    url = formataUrl(url);
    return url;
}

function formataString(string) {
    if (string != '') {
        string = string.replace(/&/g, '%26');
        string = string.replace(/=/g, '%3D');
        string = string.replace(/\?/g, '%3F');
    }
    return string;
}

function formataUrl(url) {
    url = url.replace(/\+/g, '%2B');
    url = url.replace(/ /g, '+');
    url = url.replace(/,/g, '%2c');
    url = url.replace(/\//g, '%2F');
    url = url.replace(/#/g, '%23');
    url = url.replace(/\$/g, '%24');
    url = url.replace(/;/g, '%3B');
    url = url.replace(/@/g, '%40');
    return url;
}

function pagination(tipo, link, pagina){
    
    loading('show');
    deslizar($('header'),0);

    // Inicia o objeto dados com a página da paginação
    dados = {
        pagina: (pagina < 1) ? '1' : pagina
    };

    // Se houver o select para aumentar número de itens da paginação, obtém e coloca no objeto
    if($('select[name=numero_itens]').length > 0){
        itens_pagina = $('select[name=numero_itens]').find(":selected").val();
        dados.numero_itens = itens_pagina;
    }

    // Popula outras informações dos filtros conforme o tipo
    switch (tipo) {
        case 'evento':
            dados.status = $('select[name=status]').find(":selected").val();
            dados.pesquisar = $('input[name=pesquisar]').val();
            break;
        case 'evento_produto':
            dados.evento = $('select[name=evento]').find(":selected").val();
            dados.status = $('select[name=status]').find(":selected").val();
            dados.pesquisar = $('input[name=pesquisar]').val();
            break;
        case 'usuario':
            dados.status = $('select[name=status]').find(":selected").val();
            dados.tipo = $('select[name=tipo]').find(":selected").val();
            dados.pesquisar = $('input[name=pesquisar]').val();
            break;
        case 'ticket':
            dados.status = $('select[name=status]').find(":selected").val();
            dados.evento = $('select[name=evento]').find(":selected").val();
            dados.produto = $('select[name=produto]').find(":selected").val();
            dados.pesquisar = $('input[name=pesquisar]').val();
            break;
        case 'relatorio-vendas':
            dados.status = $('select[name=status]').find(":selected").val();
            dados.pesquisar = $('input[name=pesquisar]').val();
            dados.data_inicio = $('input[name=data_inicio]').val();
            dados.data_fim = $('input[name=data_fim]').val();
            break;
        case 'relatorio-vendedor':
            dados.evento = $('select[name=evento]').find(":selected").val();
            dados.pesquisar = $('input[name=pesquisar]').val();
            dados.data_inicio = $('input[name=data_inicio]').val();
            dados.data_fim = $('input[name=data_fim]').val();
            break;
        case 'relatorio-pdv':
            dados.evento = $('select[name=evento]').find(":selected").val();
            dados.pesquisar = $('input[name=pesquisar]').val();
            dados.data_inicio = $('input[name=data_inicio]').val();
            dados.data_fim = $('input[name=data_fim]').val();
            break;
        default:
            console.log('Opa! Está sem filtros de paginação para esta página.');
    }

    var pageurl = montaUrlFiltros(dados);

    $(".content").load(link+pageurl+" .content >", function(){      
        loading('hidden');

        $("#data_inicio").datepicker({
            format: 'dd/mm/yyyy',
        });

        $("#data_fim").datepicker({
            format: 'dd/mm/yyyy',
        });
    }).fadeIn('fast');

    if (pageurl != '') {
        window.history.pushState({path:pageurl},'',pageurl);
    } else {
        window.history.pushState(null,null,link);
    }
}
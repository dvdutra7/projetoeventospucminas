function is_email(email){
	er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/; 
	if( !email.match(er) )
	{
		return false;
	}
	return true;
}

function isNumber(elemento) {  
  	elemento.val(elemento.val().replace(/[^\d]/g, ''));//Nao permite letras
}

function validaCPF(value){
	value = value.replace('.','');
	value = value.replace('.','');
	cpf = value.replace('-','');
	
	while(cpf.length < 11) cpf = "0"+ cpf;
	
	var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
	var a = [];	
	var b = new Number;	
	var c = 11;
	
	for (var i = 0; i<11; i++){
	    a[i] = cpf.charAt(i);
	    if (i < 9) b += (a[i] * --c);
	}
	
	if ((x = b % 11) < 2){ 
		a[9] = 0;
	}else{ 
		a[9] = 11-x;
	}
	
	b = 0;
	c = 11;
	
	for (var y = 0; y<10; y++) b += (a[y] * c--);
	
	if ((x = b % 11) < 2){ a[10] = 0; 
	}else{ 
		a[10] = 11-x; 
	}
	
	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) 	return false;
	
	return true;
}

function validacao(){

	jQuery.validator.addMethod("cpf", function(value) {
		return validaCPF(value);
	});

	jQuery.validator.addMethod("select_required", function(value) {
		if (value == "") {
        	return false;
       	} else {
        	return true;
       	}
	});

	jQuery.validator.addMethod("evento_pdv", function(value) {
		if (value == "" && $('select[name=usu_tipo]').find(":selected").val() == 3) {
        	return false;
       	} else {
        	return true;
       	}
	});

	jQuery.validator.addMethod("verificaTelefone", function(telefone, element) {
		var ddd = $('input[name=ddd]').val();
	
		//Array com possibilidades para o primeiro numero do celular
		var arr1 = [ 95,96,97,98,99 ];
	
		retorno = true;
		if(element.name == "telefone"){
			$('[name="telefone"]').each(function(){
				telefone = $(this).val();
				 ddd = $(this).prev().val();
				 if(ddd.substring(0,1) == 0){
					msg = 'Não existe ddd com dígito inicial igual a 0';
					retorno = false;
				}else if(telefone.substring(0,1) == 0 || telefone.substring(0,1) == 1){
					msg = 'Não existe telefone com dígito inicial igual a 0 ou 1';
					retorno = false;
				}
			});
		}else if(ddd.substring(0,1) == 0){
			msg = 'Não existe ddd com dígito inicial igual a 0';
			retorno = false;
		}else if(telefone.substring(0,1) == 0 || telefone.substring(0,1) == 1){
			msg = 'Não existe telefone com dígito inicial igual a 0 ou 1';
			retorno = false;
		}
	
		return retorno;     
	
	});

	jQuery(document).ready(function(){
		$('form').each(function () {
			jQuery(this).validate({
				errorPlacement: function(error, element) {
					var formElement = element.closest('form');
					if (element.attr("name") ==  "email") {
						error.appendTo(element.parent('div'));
					}else if (element.attr("name") ==  "senha") {
						error.appendTo(element.parent('div'));
					}else{
						error.insertAfter(formElement.find(element));
					}
				},
				// Define as regras
		        rules:{
		        	email:{
		        		required: true
		        	},
		        	senha:{
		        		required: true
		        	},
		        	nova_senha:{
		        		required: true, minlength: 6, maxlength: 60
		        	},
		        	confirmar_senha:{
		        		required: true, minlength: 6, maxlength: 60
		        	},
					usu_senha:{
						required: true, minlength: 6, maxlength: 60
					},
					eve_nome:{
						required: true, maxlength: 100
					},
					eve_local:{
						required: true, maxlength: 255
					},
					eve_cidade:{
						required: true, maxlength: 255
					},
					usu_nome:{
						required: true, maxlength: 255
					},
					usu_email:{
						required: true, maxlength: 255, email:true
					},
					cli_nome:{
						required: true, maxlength: 255
					},
					cli_email:{
						required: true, maxlength: 255, email:true
					},
					cli_cpf:{
						required: true, maxlength: 14, minlength:14, cpf: true
					},
					cli_ddd:{
						minlength: 2, maxlength: 2, digits: true
					},
					cli_telefone:{
						minlength: 9, maxlength: 9, digits: true
					},
					venda_evento:{
		        		required: true
		        	},
		        	venda_produto:{
		        		required: true
		        	},
		        },
		        // Define as mensagens de erro para cada regra
		        messages:{
		        	email:{
		        		required: 'Campo E-mail é obrigatório',
		        	},
		        	senha:{
		        		required: 'Campo Senha é obrigatório',
		        	},
					nova_senha:{
		        		required: 'Campo obrigatório',
		        		minlength: 'Tamanho mínimo 6 caracteres',
		        		maxlength: 'Tamanho máximo 60 caracteres'
		        	},
		        	confirmar_senha:{
		        		required: 'Campo obrigatório',
		        		minlength: 'Tamanho mínimo 6 caracteres',
		        		maxlength: 'Tamanho máximo 60 caracteres'
		        	},
					usu_senha:{
						required: 'Campo obrigatório',
						minlength: 'Tamanho mínimo 6 caracteres',
						maxlength: 'Tamanho máximo 60 caracteres'
					},
					eve_nome:{
						required: 'Campo obrigatório',
						maxlength: 'Tamanho máximo 100 caracteres'
					},
					eve_local:{
						required: 'Campo obrigatório',
						maxlength: 'Tamanho máximo 255 caracteres'
					},
					eve_cidade:{
						required: 'Campo obrigatório',
						maxlength: 'Tamanho máximo 255 caracteres'
					},
					usu_nome:{
						required: 'Campo obrigatório',
						maxlength: 'Tamanho máximo 255 caracteres'
					},
					usu_email:{
						required: 'Campo obrigatório',
						maxlength: 'Tamanho máximo 255 caracteres',
						email: 'Por favor insira um e-mail válido'
					},
					cli_nome:{
		        		required: 'Campo Nome é obrigatório',
		        	},
					cli_email:{
		        		required: 'Campo E-mail é obrigatório',
		        	},
		        	cli_cpf:{
		        		required: 'Campo CPF é obrigatório',
		        		cpf: "Informe um CPF válido",
		        		minlength: "Campo CPF deve conter 14 caracteres",
		                maxlength: "Campo CPF deve conter 14 caracteres",
		        	},
					cli_ddd: {
		                minlength: "No mínimo 2 dígitos",
		                maxlength: "No máximo 2 dígitos",
		                digits: "Digite um ddd válido"
					},
					cli_telefone: {
		                minlength: "No mínimo 8 dígitos",
		                maxlength: "No máximo 9 dígitos",
		                digits: "Digite um telenfone válido"
					},
					venda_evento:{
		        		required: "Campo obrigatório"
		        	},
		        	venda_produto:{
		        		required: "Campo obrigatório"
		        	},
		        },

				focusInvalid: false,
			    invalidHandler: function(form, validator) {

			        if (!validator.numberOfInvalids())
			            return;

			        $('html, body').animate({
			            scrollTop: $(validator.errorList[0].element).offset().top -80
			        }, 1000);

			    }
			});
		});
	});
}
validacao();
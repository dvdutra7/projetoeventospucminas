<?php

namespace Email;

return array(
    'view_manager' => [
        'template_path_stack' => [
            'email' => __DIR__ . '/../view',
        ],
    ],
);
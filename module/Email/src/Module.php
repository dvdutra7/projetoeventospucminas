<?php

namespace Email;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
    	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'SendGrid'::class => function($container) {
                    return new Service\SendGrid($container);
                },
            ),          
        );
    }
}
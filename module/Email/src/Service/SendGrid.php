<?php

namespace Email\Service;

use Zend\Mail;  
use Zend\Mime\Part as MimePart;  
use Zend\Mime\Message as MimeMessage;
use Zend\View\Model\ViewModel;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Crypt\PublicKey\Rsa\PublicKey;

class SendGrid
{
	protected $service_locator;	

	private $options = array();
	
	public function __construct($container)
	{
		$this->service_locator = $container;
		$config = $container->get('config');

		$this->options = [
			'api_key' => $config['ambiente']['api_key'],
			'mail_from' => $config['ambiente']['mail_from'],
			'mail_backup' => $config['ambiente']['mail_backup'],
			'name_from' => 'David Dutra'
		];
	}
	
	public function sendEmail($content, $to, $subject, $type = null, $replyto = array(), $cc = array(), $mail_from = null, $name_from = null, $bcc = array())
	{
		// se mail_from estiver vazio, recupera o padrão do sistema
		if(empty($mail_from)){
			$mail_from = $this->options['mail_from'];
		}

		// se name_from estiver vazio, recupera o padrão do sistema
		if(empty($name_from)){
			$name_from = $this->options['name_from'];
		}			

		$email = new \SendGrid\Mail\Mail(); 
		$email->setFrom($mail_from, $name_from);
		$email->setSubject($subject);
		foreach ($to as $cli_email => $name)
		{
			$email->addTo("{$cli_email}", "{$name}");
		}

		//Copia oculta de email que amazena os emails enviados pela sendgrid
		$email->AddBCC($this->options['mail_backup'], 'Sistema Pós PUC');

		$email->addContent("text/html", $content);
		$sendgrid = new \SendGrid($this->options['api_key']);
		try {
		    $response = $sendgrid->send($email);
		    
		    return $response;
		    /*print $response->statusCode() . "\n";
		    print_r($response->headers());
		    print $response->body() . "\n";*/
		} catch (Exception $e) {
		    echo 'Caught exception: '. $e->getMessage() ."\n";
		}
	}

	public function montaLayout($dados, $content = 'default', $layout = 'default', $head = 'default', $footer = 'default')
	{
		$tpl = array('content' => $content, 'head' => $head, 'footer' => $footer);
		
		//Merge no array de configuracao do template com dados do email
		$dados_tpl = array_merge($dados, $tpl);
		
		$renderer = $this->service_locator->get('Zend\View\Renderer\RendererInterface');
		
		return $renderer->render('email/layout/'.$layout, $dados_tpl);
	}
}

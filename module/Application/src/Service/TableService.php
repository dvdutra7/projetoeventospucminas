<?php

namespace Application\Service;

use Zend\Db\Adapter\AdapterInterface;

/**
 * Servico responsavel pela autenticacao da aplicao
 * @package Service
 */
class TableService
{
    private $sm;
    private $adapter;

    //tables
    private $usuario_table;
    private $usuario_tipo_table;
    private $usuario_evento_table;
    private $usuario_sessao_table;
    private $evento_table;
    private $evento_produto_table;
    private $cliente_table;
    private $ticket_table;

    public function __construct($container){
        $this->sm = $container;
        $this->adapter = $container->get(AdapterInterface::class);
    }

    /* inicia uma transaction */
    public function begin()
    {
        $this->adapter->getDriver()->getConnection()->beginTransaction();
    }

    /* envia uma transaction */
    public function commit()
    {
        $this->adapter->getDriver()->getConnection()->commit();
    }

    /* desfaz uma transaction */
    public function rollback()
    {
        $this->adapter->getDriver()->getConnection()->rollback();
    }

    public function getUsuarioTable()
    {
        if(!$this->usuario_table){
            $this->usuario_table = new \PainelAdmin\Model\UsuarioTable($this->sm);
        }
        return $this->usuario_table;
    }

    public function getUsuarioTipoTable()
    {
        if(!$this->usuario_tipo_table){
            $this->usuario_tipo_table = new \PainelAdmin\Model\UsuarioTipoTable($this->sm);
        }
        return $this->usuario_tipo_table;
    }

    public function getUsuarioEventoTable()
    {
        if(!$this->usuario_evento_table){
            $this->usuario_evento_table = new \PainelAdmin\Model\UsuarioEventoTable($this->sm);
        }
        return $this->usuario_evento_table;
    }

    public function getUsuarioSessaoTable()
    {
        if(!$this->usuario_sessao_table){
            $this->usuario_sessao_table = new \PainelAdmin\Model\UsuarioSessaoTable($this->sm);
        }
        return $this->usuario_sessao_table;
    }

    public function getEventoTable()
    {
        if(!$this->evento_table){
            $this->evento_table = new \PainelAdmin\Model\EventoTable($this->sm);
        }
        return $this->evento_table;
    }

    public function getEventoProdutoTable()
    {
        if(!$this->evento_produto_table){
            $this->evento_produto_table = new \PainelAdmin\Model\EventoProdutoTable($this->sm);
        }
        return $this->evento_produto_table;
    }

    public function getClienteTable()
    {
        if(!$this->cliente_table){
            $this->cliente_table = new \PainelAdmin\Model\ClienteTable($this->sm);
        }
        return $this->cliente_table;
    }

    public function getTicketTable()
    {
        if(!$this->ticket_table){
            $this->ticket_table = new \PainelAdmin\Model\TicketTable($this->sm);
        }
        return $this->ticket_table;
    }
}
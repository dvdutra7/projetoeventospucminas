<?php
namespace Application;

use Zend\Mvc\MvcEvent;
use Application\Listener\LayoutListener;

class Module
{
    const VERSION = '3.1.3';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event) : void
    {
        $application = $event->getApplication();

        /** @var TemplateMapResolver $templateMapResolver */
        $templateMapResolver = $application->getServiceManager()->get(
            'ViewTemplateMapResolver'
        );

        // Create and register layout listener
        $listener = new LayoutListener($templateMapResolver);
        $listener->attach($application->getEventManager());
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'tables' => function($container) {
                    return new Service\TableService($container);
                },
            ],
        ];
    }
}

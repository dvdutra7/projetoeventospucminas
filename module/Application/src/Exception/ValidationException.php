<?php

namespace Application\Exception;

/**
 * Exceção lançada quando ocorre algum erro de validação
 */
class ValidationException extends \Exception {

}
<?php 
namespace PainelAdmin\Entity;

class UsuarioSessao
{
    public $uss_id;
    public $uss_usu_id;
    public $uss_hash;
    public $uss_data_expiracao;

    public function exchangeArray($dados)
    {
        foreach($dados as $key => $dado){
            $this->$key = $dado;
        }
    }

    public function setUsuario($value)
    {
        $this->uss_usu_id = $value;
    }

    public function setHash($value)
    {
        $this->uss_hash = $value;
    }
    
    public function setDataExpiracao($value)
    {
        $this->uss_data_expiracao = $value;  
    }
}

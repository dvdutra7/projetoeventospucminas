<?php 
namespace PainelAdmin\Entity;

class UsuarioTipo
{
    public $ust_id;
    public $ust_nome;

    public function exchangeArray($dados)
    {
        foreach($dados as $key => $dado){
            $this->$key = $dado;
        }
    }
}

<?php 
namespace PainelAdmin\Entity;

class Evento
{
    public $eve_id;
    public $eve_nome;
    public $eve_ativo;
    public $eve_local;
    public $eve_cidade;
    public $eve_data_cadastro;

    public function exchangeArray($dados)
    {
        foreach($dados as $key => $dado){
            $this->$key = $dado;
        }
    }
}

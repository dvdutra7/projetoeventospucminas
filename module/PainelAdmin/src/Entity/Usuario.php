<?php 
namespace PainelAdmin\Entity;

class Usuario
{
    public $usu_id;
    public $usu_ust_id;
    public $usu_nome;
    public $usu_email;
    public $usu_senha;

    public function exchangeArray($dados)
    {
        foreach($dados as $key => $dado){
            $this->$key = $dado;
        }
    }

    public function setNome($value)
    {
        $this->usu_nome = $value;
    }

    public function setSenha($value)
    {
        $this->usu_senha = md5($value);
    }

    public function setTipo($value)
    {
        $this->usu_ust_id = $value;
    }

    public function setEmail($value)
    {
        $this->usu_email = $value;  
    }

    public function setFormObject($data)
    {
        $this->usu_nome = $data['nome'];
        $this->usu_email = $data['email'];
    }
}

<?php 
namespace PainelAdmin\Entity;

class UsuarioEvento
{
    public $use_id;
    public $use_usu_id;
    public $use_eve_id;
    public $use_ativo;

    public function exchangeArray($dados)
    {
        foreach($dados as $key => $dado){
            $this->$key = $dado;
        }
    }
}

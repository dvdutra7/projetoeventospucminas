<?php 
namespace PainelAdmin\Entity;

class Cliente
{
    public $cli_id;
    public $cli_nome;
    public $cli_email;
    public $cli_cpf;
    public $cli_ddd;
    public $cli_telefone;
    public $cli_data_cadastro;

    public function exchangeArray($dados)
    {
        foreach($dados as $key => $dado){
            $this->$key = $dado;
        }
    }
}

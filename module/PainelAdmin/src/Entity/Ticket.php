<?php 
namespace PainelAdmin\Entity;

class Ticket
{
    public $tic_id;
    public $tic_cli_id;
    public $tic_evp_id;
    public $tic_usu_id;
    public $tic_cancelado;
    public $tic_liberado_por;
    public $tic_data_entrada;
    public $tic_data_cadastro;

    public function exchangeArray($dados)
    {
        foreach($dados as $key => $dado){
            $this->$key = $dado;
        }
    }
}

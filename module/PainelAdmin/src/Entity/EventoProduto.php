<?php 
namespace PainelAdmin\Entity;

class EventoProduto
{
    public $evp_id;
    public $evp_eve_id;
    public $evp_nome;
    public $evp_excluido;
    public $evp_data_cadastro;

    public function exchangeArray($dados)
    {
        foreach($dados as $key => $dado){
            $this->$key = $dado;
        }
    }
}

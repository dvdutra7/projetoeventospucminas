<?php

namespace PainelAdmin\Service;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Db\Adapter\AdapterInterface;

use PainelAdmin\Entity\UsuarioSessao;

/**
 * Servico responsavel pela autenticacao da aplicao
 * @package Service
 */
class Autenticacao
{
    private $db_adapter;
    private $session;

    private $sm;

    /** 
     * Construtor da classe
     *
     * @return void
     */
    public function __construct($container)
    {
        $this->db_adapter = $container->get(AdapterInterface::class);

        $this->sm = $container;

        $this->session = $container->get('Session');
    }

    public function autenticaoAtiva(){
        $auth = new AuthenticationService();
        return $auth->hasIdentity();
    }

    /**
     * Faz a autenticao dos usuarios afiliados
     * 
     * @param array $params
     * @return array
     */
    public function autenticar($params)
    {
        if (!isset($params['email']) || !isset($params['senha'])) {
            throw new \Exception("Parâmetros inválidos");
        }

        $password = md5($params['senha']);

        $user = $this->fazerLogin($params['email'], $password);

        if(!$user){
            return false;
        }

        // salva o user na sessao
        $this->session->offsetSet('user', $user);   

        $tb = $this->sm->get('tables');

        // se o checkbox 'Mantenha Conectado' for marcado, mantém a sessão por 1 mes
        if(isset($params['mantenha_conectado']) && $params['mantenha_conectado'] == 'true') {
            $cookie_time = time() + (3600*24*30*1);
        } else {
            // Salva por 1 dia
            $cookie_time = time() + (3600*24*1);
        }

        if(!is_null($cookie_time)) {

            $cookie_value = md5('tol'.$user->usu_nome.$_SERVER['HTTP_USER_AGENT'].$_SERVER['REMOTE_ADDR'].'tol');

            // Cria o cookie para poder relogar o usuário futuramente
            setcookie('manter_logado', $cookie_value, $cookie_time, '/');
            setcookie('cliente_logado', base64_encode($user->usu_id.'-'.'tol'), $cookie_time, '/');

             // salvar na tabela de sessao
            $user_session = new UsuarioSessao();
            $user_session->setHash($cookie_value);
            $user_session->setUsuario($user->usu_id);

            $tb->getUsuarioSessaoTable()->salvarSessao($user_session);
        }
        
        return true;
    }

    public function fazerLogin($email, $password)
    {
        $auth = new AuthenticationService();
        $auth_adapter = new AuthAdapter($this->db_adapter);
        $auth_adapter
            ->setTableName('usuario')
            ->setIdentityColumn('usu_email')
            ->setCredentialColumn('usu_senha')
            ->setIdentity($email)
            ->setCredential($password);

        $result = $auth->authenticate($auth_adapter);

        return $auth_adapter->getResultRowObject();
    }

    public function revalidarSessao($id)
    {
        $tb = $this->sm->get('tables');
        $user = $tb->getUsuarioTable()->getUsuarioById($id);

        $this->session->offsetSet('user', $user);  
    }

    public function logout() {
        $auth = new AuthenticationService();

        $this->session->offsetUnset('user');
        $this->session->offsetUnset('configuracao');
        $auth->clearIdentity();

        //matar sessao
        $tb = $this->sm->get('tables');

        if(isset($_COOKIE['cliente_logado'])) {
            $cookie_id = base64_decode($_COOKIE['cliente_logado']);
            $aux = explode('-', $cookie_id);

            $cookie_clid = $aux[0];

            $tb->getUsuarioSessaoTable()->deleteSessao($cookie_clid, $_COOKIE['manter_logado']);
        }

        // Remove os cookies
        setcookie('manter_logado', '', time() - 3600, '/');
        setcookie('cliente_logado', '', time() - 3600, '/');

        return true;
    }
}
<?php
namespace PainelAdmin\Form;

use Zend\Form\Form;

class SenhaForm extends Form
{
    private $sm;

    public function __construct()
    {
        parent::__construct('alterar-senha');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/painel/usuarios/salvar-senha');


        $this->add([
            'name' => 'nova_senha',
            'type' => 'password',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Nova senha',
                'required' => 'required',
                'id' => 'nova_senha'
            ],
            'options' => [
                'label' => 'Nova senha'
            ]
        ]);

        $this->add([
            'name' => 'confirmar_senha',
            'type' => 'password',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Confirmar nova senha',
                'required' => 'required'
            ],
            'options' => [
                'label' => 'Confirmar nova senha'
            ]
        ]);

        $this->add([
            'name' => 'usu_senha',
            'type' => 'password',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Sua senha',
                'required' => 'required'
            ],
            'options' => [
                'label' => 'Sua senha'
            ]
        ]);
    }
}
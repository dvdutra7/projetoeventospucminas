<?php
namespace PainelAdmin\Form;

use Zend\Form\Form;

class EventoProdutoForm extends Form
{
    private $sm;

    public function __construct($form_id = 'form-evento-produto', $sm = '')
    {
        parent::__construct($form_id);
        $this->sm = $sm;
        $this->setAttribute('method', 'post');

        $this->add([
            'name' => 'evp_id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'evp_eve_id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'evp_nome',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Nome',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'Nome'
            ]
        ]);

        $this->add([
            'name' => 'evp_preco',
            'type' => 'text',
            'attributes' => [
                'id' => 'preco',
                'class' => 'form-control',
                'placeholder' => 'Preço',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'Preço'
            ]
        ]);

    }

    public function populateForm($evento_produto)
    {
        $evento_produto = (array) $evento_produto;
        $this::populateValues($evento_produto);
    }
}
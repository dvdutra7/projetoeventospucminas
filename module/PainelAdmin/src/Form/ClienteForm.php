<?php
namespace PainelAdmin\Form;

use Zend\Form\Form;

class ClienteForm extends Form
{
    private $sm;

    public function __construct($form_id = 'form-cliente')
    {
        parent::__construct($form_id);
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/painel/clientes/cadastrar');

        $this->add([
            'name' => 'cli_id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'cli_nome',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Nome',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'Nome'
            ]
        ]);

        $this->add([
            'name' => 'cli_email',
            'type' => 'Email',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'E-mail',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'E-mail'
            ]
        ]);

         $this->add([
            'type'  => 'text',
            'name' => 'cli_cpf',
            'attributes' => [
                'class' => 'form-control cpf-mask',
                'required' => 'required',
                'placeholder' => 'CPF',
                'maxlength' => '14',
            ],
            'options' => [
                'label' => 'CPF',
            ],
        ]);

        $this->add([
            'type' => 'Text',
            'name' => 'cli_ddd',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'DDD',
                'pattern' => '[1-9]{2}',
                'maxlength' => '2',
            ],
            'options' => [
                'label' => 'DDD',
            ]
        ]);
        
        $this->add(array(
            'type' => 'Text',
            'name' => 'cli_telefone',
            'attributes' => array( 
                'class' => 'form-control',
                'placeholder' => 'Telefone',
                'maxlength' => '9',
            ),
            'options' => array(
                'label' => 'Telefone',   
            )
        ));
    }

    public function populateForm($cliente)
    {
        $cliente = (array) $cliente;
 
        $this::populateValues($cliente);
    }
}
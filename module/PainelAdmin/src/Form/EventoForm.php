<?php
namespace PainelAdmin\Form;

use Zend\Form\Form;

class EventoForm extends Form
{
    private $sm;

    public function __construct($form_id = 'form-evento', $sm = '')
    {
        parent::__construct($form_id);
        $this->sm = $sm;
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/painel/eventos/cadastrar');

        $this->add([
            'name' => 'eve_id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'eve_nome',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Nome',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'Nome'
            ]
        ]);

        $this->add([
            'name' => 'eve_local',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Local',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'Local'
            ]
        ]);

        $this->add([
            'name' => 'eve_cidade',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Cidade',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'Cidade'
            ]
        ]);
    }

    public function populateForm($evento)
    {
        $evento = (array) $evento;
        $this::populateValues($evento);
    }
}
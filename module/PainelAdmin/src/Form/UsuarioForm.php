<?php
namespace PainelAdmin\Form;

use Zend\Form\Form;

class UsuarioForm extends Form
{
    private $sm;

    public function __construct($sm, $form_id = 'form-usuario')
    {
        parent::__construct($form_id);
        $this->sm = $sm;
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/painel/usuarios/cadastrar');

        $this->add([
            'name' => 'usu_id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'usu_nome',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Nome',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'Nome'
            ]
        ]);

        $this->add([
            'name' => 'usu_email',
            'type' => 'Email',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'E-mail',
                'required' => 'required',
            ],
            'options' => [
                'label' => 'E-mail'
            ]
        ]);

        $this->add(array(
            'type' => 'Select',
            'name' => 'usu_ust_id',
            'attributes' => array(
                'required' => 'required',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Tipo de Usuário',
                'options' => $this->populateUsuarioTipo()
            )
        ));
    }

    private function populateUsuarioTipo()
    {
        $usuario_tipo = $this->sm->get('tables')->getUsuarioTipoTable()->getUsuarioTipos();

        $array[''] = 'Selecione um Tipo...';

        foreach($usuario_tipo as $tipo){
            $array[$tipo->ust_id] = $tipo->ust_nome;
        }
        
        return $array;
    }

    public function populateForm($usuario)
    {
        $usuario = (array) $usuario;

        $this::populateValues($usuario);
    }
}
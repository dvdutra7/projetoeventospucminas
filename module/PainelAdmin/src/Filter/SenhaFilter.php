<?php
namespace PainelAdmin\Filter;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SenhaFilter implements InputFilterAwareInterface

{
    private $input_filter;

    public function setInputFilter(InputFilterInterface $input_filter)
    {
        throw new DomainException(sprintf(
            '%s does not allow injection of an alternate input filter',
            __CLASS__
        ));
    }

    public function getInputFilter()
    {	
        if ($this->input_filter)
        {
            return $this->input_filter;
        }

        $input_filter = new InputFilter();

        $input_filter->add(array(
            'name'     => 'nova_senha',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array (
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Este campo é obrigatório',
                        )
                    ),
                ),
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 6,
                        'max'      => 60,
                    ),
                ),
            ),
        ));

        $input_filter->add(array(
            'name'     => 'confirmar_senha',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array (
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Este campo é obrigatório',
                        )
                    ),
                ),
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 6,
                        'max'      => 60,
                    ),
                ),
                array(
                    'name' => 'Identical',
                    'options' => array(
                        'token' => 'nova_senha',
                    ),
                ),
            ),
        ));

        $input_filter->add(array(
            'name'     => 'usu_senha',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array (
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Este campo é obrigatório',
                        )
                    ),
                ),
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 6,
                        'max'      => 60,
                    ),
                ),
            ),
        ));

        $this->input_filter = $input_filter;

        return $this->input_filter;
    }
}
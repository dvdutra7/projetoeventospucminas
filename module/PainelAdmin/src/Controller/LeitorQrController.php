<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class LeitorQrController extends AbstractActionController
{
	private $sm;
	private $user;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();

        $this->user = $this->sm->get('Session')->offsetGet('user');

        if (!$this->user || !in_array($this->user->usu_ust_id, ['1', '2'])) {
            return $this->redirect()->toRoute('login');
        }
	}

    public function indexAction()
    {
        return new ViewModel();
    }
}

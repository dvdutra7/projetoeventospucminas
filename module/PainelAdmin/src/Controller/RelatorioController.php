<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class RelatorioController extends AbstractActionController
{
	private $sm;
	private $user;
    private $tables;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        if (!$this->user || $this->user->usu_ust_id != 1) {
            return $this->redirect()->toRoute('login');
        }

        $inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
        $inlineScript->appendFile('/js/geral.js');
        $inlineScript->appendFile('/js/relatorio.js');
	}

    public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';
        $status = (isset($dados->status)) ? $dados->status : '';
        $data_inicio = (isset($dados->data_inicio)) ? $dados->data_inicio : '';
        $data_fim = (isset($dados->data_fim)) ? $dados->data_fim : '';

        if($data_inicio != ''){
            $data_inicio = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $data_inicio)));
        }

        if($data_fim != ''){
            $data_fim = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $data_fim)));
        }

        $usuarios = $this->tables->getTicketTable()->getVendasTotaisPdv($pesquisar, $status, $data_inicio, $data_fim);

        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($usuarios);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

        return new ViewModel([
            'resultados' => $paginator,
            'dados' => $dados,
        ]);
    }

    public function vendedorAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $vendedor_id = $this->params('id');

        $vendedor = $this->tables->getUsuarioTable()->getUsuarioById($vendedor_id);

        $pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';
        $evento = (isset($dados->evento)) ? $dados->evento : '';
        $data_inicio = (isset($dados->data_inicio)) ? $dados->data_inicio : '';
        $data_fim = (isset($dados->data_fim)) ? $dados->data_fim : '';

        if($data_inicio != ''){
            $data_inicio = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $data_inicio)));
        }

        if($data_fim != ''){
            $data_fim = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $data_fim)));
        }

        $vendas_produtos = $this->tables->getTicketTable()->getVendasByPdv($vendedor_id, $pesquisar, $data_inicio, $data_fim, $evento);

        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($vendas_produtos);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

        $eventos = $this->tables->getEventoTable()->getEventosByPdv($vendedor_id);

        return new ViewModel([
            'vendedor' => $vendedor,
            'resultados' => $paginator,
            'dados' => $dados,
            'eventos' => $eventos,
        ]);
    }

    public function modalDetalhesAction()
    {
        $view = new ViewModel();

        $request = $this->getRequest();

        $post = $request->getPost();

        // Se for post e ajax
        if ($request->isPost() && $request->isXmlHttpRequest()) {

            $vendas_produtos = $this->tables->getTicketTable()->getVendasByPdvAndProduto($post['usuario_id'], $post['produto_id']);

            $view->setTerminal(true);
            $view->setVariables([
                'vendas_produtos' => $vendas_produtos,
            ]);
        }

        return $view;
    }
}

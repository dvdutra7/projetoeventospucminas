<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use PainelAdmin\Form\EventoForm;
use PainelAdmin\Entity\Evento;

use Application\Exception\ValidationException;

class EventoController extends AbstractActionController
{
    private $env;
	private $sm;
	private $user;
    private $tables;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        $paginasParaNaoRedirecionar = [];

        if ((!$this->user || $this->user->usu_ust_id != 1) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
            return $this->redirect()->toRoute('login');
        }

        $inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
        $inlineScript->appendFile('/js/evento.js');
	}

    public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';
        $status = (isset($dados->status)) ? $dados->status : '';

        $eventos = $this->tables->getEventoTable()->getEventos($pesquisar, $status);

        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($eventos);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

        return new ViewModel([
            'eventos' => $paginator,
            'dados' => $dados,
        ]);
    }

    public function cadastrarAction()
    {

        $request = $this->getRequest();

        if($request->isXmlHttpRequest() && $request->isPost()) {

            $response = $this->getResponse();

            $post = $request->getPost(); 
			parse_str($post['dados'], $dados);

            $evento = new Evento();
            $evento->exchangeArray($dados);
            $evento->eve_id = $this->tables->getEventoTable()->salvarEvento($evento);

            $retorno['sucesso'] = true;
            $retorno['path'] = '/painel/eventos';
            $retorno['mensagem'] = 'Evento cadastrado com sucesso';

            $response->setContent(\Zend\Json\Json::encode($retorno));
            return $response;

        } else {
            $form = new EventoForm('form-evento', $this->sm);
            
            return new ViewModel([
                'form' => $form,
                'textButton' => 'Cadastrar'
            ]);
        }
    }

    public function editarAction()
    {
        $id =  $this->params()->fromRoute('id'); 
        $request = $this->getRequest();

        $evento = $this->tables->getEventoTable()->getEventoById($id);

        if($request->isXmlHttpRequest() && $request->isPost()) {

            $response = $this->getResponse();

            $post = $request->getPost(); 
            parse_str($post['dados'], $dados);

            $evento->exchangeArray($dados);
            $this->tables->getEventoTable()->salvarEvento($evento);

            $retorno['sucesso'] = true;
            $retorno['mensagem'] = 'Evento editado com sucesso';

            $response->setContent(\Zend\Json\Json::encode($retorno));
            return $response;

        } else {
            $form = new EventoForm('form-evento', $this->sm);
			$form->populateForm($evento);

            $form->setAttribute('action', '/painel/eventos/editar/'.$id);

            $view = new ViewModel();
            $view->setTemplate('painel-admin/evento/cadastrar');

            $view->setVariables([
                'form' => $form,
                'textButton' => 'Editar'
            ]);

            return $view;
        }
    }

    public function ativarInativarEventoAction()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $retorno['sucesso'] = false;
        $retorno['validacao'] = false;
        
        if($request->isXmlHttpRequest() && $request->isPost()) {
            
            try{
                $post = $request->getPost(); 
            
                $evento = $this->tables->getEventoTable()->getEventoById($post->id);

                if(!$evento){
                    throw new ValidationException('Evento não encontrado!');
                }

                if(!$evento->eve_ativo){
                    $evento_produtos = $this->tables->getEventoProdutoTable()->getEventoProdutos('', $evento->eve_id);

                    if(count($evento_produtos) < 1){
                        throw new ValidationException('Não é possível ativar um evento sem produtos!');
                    }
                }

                if($evento->eve_ativo){
                    $this->tables->getEventoTable()->desativarEvento($post->id);
                } else {
                    $this->tables->getEventoTable()->ativarEvento($post->id);
                }
                
                $retorno['sucesso'] = true;
                $retorno['mensagem'] = 'Evento editado com sucesso';
            } catch (ValidationException $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = true;
                $retorno['mensagem'] = $e->getMessage();
            } catch (\Exception $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = false;
                $retorno['mensagem'] = $e->getMessage();

                if(strpos($_SERVER['HTTP_HOST'], "localhost") !== false){
                    $retorno['interno'] = true;
                }
            }
        }

        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }
}

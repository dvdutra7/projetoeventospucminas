<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use PainelAdmin\Form\EventoProdutoForm;
use PainelAdmin\Entity\EventoProduto;

use Application\Exception\ValidationException;

class EventoProdutoController extends AbstractActionController
{
    private $env;
	private $sm;
	private $user;
    private $tables;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        $paginasParaNaoRedirecionar = [];

        if ((!$this->user || $this->user->usu_ust_id == 2) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
            return $this->redirect()->toRoute('login');
        }

        $inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
        $inlineScript->appendFile('/js/evento-produto.js');
	}

    public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $evento_id = $this->params('evento_id');

        $evento = $this->tables->getEventoTable()->getEventoById($evento_id);

        $pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';
        $status = (isset($dados->status)) ? $dados->status : '';

        $evento_produtos = $this->tables->getEventoProdutoTable()->getEventoProdutos($pesquisar, $evento_id);

        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($evento_produtos);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

        return new ViewModel([
            'evento' => $evento,
            'evento_produtos' => $paginator,
            'dados' => $dados,
        ]);
    }

    public function cadastrarAction()
    {

        $request = $this->getRequest();

        $evento_id = $this->params('evento_id');

        $evento = $this->tables->getEventoTable()->getEventoById($evento_id);

        if($evento){
            if($request->isXmlHttpRequest() && $request->isPost()) {

                $response = $this->getResponse();

                $post = $request->getPost(); 
                parse_str($post['dados'], $dados);

                $evento_produto = new EventoProduto();
                $evento_produto->exchangeArray($dados);

                $evento_produto->evp_id = $this->tables->getEventoProdutoTable()->salvarEventoProduto($evento_produto);
                $retorno['sucesso'] = true;
                $retorno['path'] = '/painel/eventos/'.$evento->eve_id.'/produtos';
                $retorno['mensagem'] = 'Produto cadastrado com sucesso';

                $response->setContent(\Zend\Json\Json::encode($retorno));
                return $response;

            } else {
                $form = new EventoProdutoForm('form-evento-produto', $this->sm);

                $form->setAttribute('action', '/painel/eventos/'.$evento->eve_id.'/produtos/cadastrar/');

                $form->populateForm(['evp_eve_id' => $evento->eve_id]);
                
                return new ViewModel([
                    'evento' => $evento,
                    'form' => $form,
                    'textButton' => 'Cadastrar'
                ]);
            }
        }

        $this->getResponse()->setStatusCode(404);
        return;
    }

    public function editarAction()
    {
        $id =  $this->params()->fromRoute('id'); 
        $request = $this->getRequest();

        $evento_id = $this->params('evento_id');

        $evento = $this->tables->getEventoTable()->getEventoById($evento_id);
        $evento_produto = $this->tables->getEventoProdutoTable()->getEventoProdutoById($id);

        if($request->isXmlHttpRequest() && $request->isPost()) {

            $response = $this->getResponse();

            $post = $request->getPost(); 
            parse_str($post['dados'], $dados);

            $evento_produto->exchangeArray($dados);
            $this->tables->getEventoProdutoTable()->salvarEventoProduto($evento_produto);

            $retorno['sucesso'] = true;
            $retorno['mensagem'] = 'Produto editado com sucesso';

            $response->setContent(\Zend\Json\Json::encode($retorno));
            return $response;

        } else {
            $form = new EventoProdutoForm('form-evento-produto', $this->sm);
			$form->populateForm($evento_produto);

            $form->setAttribute('action', '/painel/eventos/'.$evento->eve_id.'/produtos/editar/'.$id);

            $view = new ViewModel();
            $view->setTemplate('painel-admin/evento-produto/cadastrar');

            $view->setVariables([
                'evento' => $evento,
                'form' => $form,
                'textButton' => 'Editar'
            ]);

            return $view;
        }
    }

    public function excluirEventoProdutoAction()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $retorno['sucesso'] = false;
        $retorno['validacao'] = false;
        
        if($request->isXmlHttpRequest() && $request->isPost()) {
            
            try{
                $post = $request->getPost(); 

                $evento_id = $this->params('evento_id');
                $evento = $this->tables->getEventoTable()->getEventoById($evento_id);
            
                if(!$evento){
                    throw new ValidationException('Evento não encontrado!');
                }

                $evento_produto = $this->tables->getEventoProdutoTable()->getEventoProdutoById($post->id);

                if(!$evento_produto){
                    throw new ValidationException('Produto não encontrado!');
                }

                if($evento_produto->evp_eve_id != $evento->eve_id){
                    throw new ValidationException('Algo estranho aconteceu, atualize a página e tente novamente!');
                }

                $this->tables->getEventoProdutoTable()->excluirEventoProduto($post->id);
                
                $retorno['sucesso'] = true;
                $retorno['mensagem'] = 'Produto excluído com sucesso';
            } catch (ValidationException $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = true;
                $retorno['mensagem'] = $e->getMessage();
            } catch (\Exception $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = false;
                $retorno['mensagem'] = $e->getMessage();

                if(strpos($_SERVER['HTTP_HOST'], "localhost") !== false){
                    $retorno['interno'] = true;
                }
            }
        }

        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }
}

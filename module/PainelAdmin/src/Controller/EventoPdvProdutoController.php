<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use PainelAdmin\Form\EventoProdutoForm;
use PainelAdmin\Entity\EventoProduto;

use Application\Exception\ValidationException;

class EventoPdvProdutoController extends AbstractActionController
{
    private $env;
	private $sm;
	private $user;
    private $tables;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        $paginasParaNaoRedirecionar = [];

        if ((!$this->user || $this->user->usu_ust_id == 2) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
            return $this->redirect()->toRoute('login');
        }

        $inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
        $inlineScript->appendFile('/js/evento-pdv.js');
	}

    public function indexAction()
    {
        $request = $this->getRequest();

        $evento_id = $this->params('evento_id');

        $evento = $this->tables->getEventoTable()->getEventoById($evento_id);

        $evento_produtos = $this->tables->getEventoProdutoTable()->getEventoProdutos('', $evento_id);

        return new ViewModel([
            'evento' => $evento,
            'evento_produtos' => $evento_produtos
        ]);
    }
}

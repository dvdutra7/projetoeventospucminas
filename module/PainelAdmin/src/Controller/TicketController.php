<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use DOMPDFModule\View\Model\PdfModel;

class TicketController extends AbstractActionController
{
    private $env;
	private $sm;
	private $user;
    private $tables;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        $this->env = getenv('APPLICATION_ENV') ?: 'production';

        $paginasParaNaoRedirecionar = [
            'entrada',
            'liberar-entrada',
        ];

        if ((!$this->user || $this->user->usu_ust_id != 1) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
            return $this->redirect()->toRoute('login');
        }

        $inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
        $inlineScript->appendFile('/js/geral.js');
        $inlineScript->appendFile('/js/ticket.js');
	}

    public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';
        $produto = (isset($dados->produto)) ? $dados->produto : '';
        $status = (isset($dados->status)) ? $dados->status : '';
        $evento = (isset($dados->evento)) ? $dados->evento : '';

        if($evento == ''){
            $produto = '';
        }

        $eventos = $this->tables->getEventoTable()->getEventos('', 1);
        $produtos = $this->tables->getEventoProdutoTable()->getEventoProdutos('', $evento);
        $tickets = $this->tables->getTicketTable()->getTickets($pesquisar, $status, $evento, $produto);

        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($tickets);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

        return new ViewModel([
            'tickets' => $paginator,
            'produtos' => $produtos,
            'dados' => $dados,
            'eventos' => $eventos,
        ]);
    }

    public function entradaAction()
    {
        // captura a requisicao
        $request = $this->getRequest();

        $id = $this->params('id');

        if(is_numeric($id)){

            $ticket = $this->tables->getTicketTable()->getTicketCompletoById($id);

            if($ticket){
                $evento = $this->tables->getEventoTable()->getEventoById($ticket->evp_eve_id);

                if(isset($post['leitor'])){
                    $leitor = true;
                }else{
                    $leitor = false;
                }

                $view = new ViewModel([
                    'ticket' => $ticket,
                    'evento' => $evento,
                    'leitor' => $leitor,
                ]);

                // recupera os dados do post
                $post = $request->getPost();

                if(isset($post['leitor'])){
                    $view->setTerminal(true);
                }

                if (!$this->user) {
                    $this->layout()->setTemplate('layout/no-layout');
                    $view->setTemplate('painel-admin/ticket/ticket-cliente');
                }else{
                    $view->setTemplate('painel-admin/ticket/ticket-admin');
                }

                return $view;
            }else{
                $view = new ViewModel([
                   
                ]);

                if (!$this->user) {
                    $this->getResponse()->setStatusCode(404);
                    return;
                }else{
                    $view->setTemplate('painel-admin/ticket/ticket-invalido-admin');
                }

                return $view;
            }
        }

        $this->getResponse()->setStatusCode(404);
        return;
    }

    public function liberarEntradaAction()
    {
        // recupera requisição e a resposta
        $request = $this->getRequest();
        $response = $this->getResponse();

        $retorno['salvou'] = false;

        // Se for post e ajax
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            try{
                // recupera os dados do post
                $post = $request->getPost();

                $ticket = $this->tables->getTicketTable()->getTicketCompletoById($post['ticket_id']);

                $ticket->tic_entrou_evento = 1;
                $ticket->tic_data_entrada = date('Y-m-d H:i:s');
                $ticket->tic_liberado_por = $this->user->usu_id;

                $this->tables->getTicketTable()->salvarTicket($ticket);

                $retorno['salvou'] = true;

            } catch (\Exception $e) {
                $retorno['salvou'] = false;
                $retorno['mensagem'] = $e->getMessage();
            }
        }
        
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    public function modalDetalhesAction()
    {
        $view = new ViewModel();

        $request = $this->getRequest();

        $post = $request->getPost();

        // Se for post e ajax
        if ($request->isPost() && $request->isXmlHttpRequest()) {

            $ticket = $this->tables->getTicketTable()->getTicketCompletoById($post['ticket_id']);

            $view->setTerminal(true);
            $view->setVariables([
                'ticket' => $ticket,
            ]);
        }

        return $view;
    }

    public function cancelarTicketAction()
    {
        // recupera requisição e a resposta
        $request = $this->getRequest();
        $response = $this->getResponse();

        $retorno['salvou'] = false;

        // Se for post e ajax
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            try{
                // recupera os dados do post
                $post = $request->getPost();

                $ticket = $this->tables->getTicketTable()->getTicketById($post['id']);

                if(!$ticket){
                    throw new \Exception("Ticket não encontrado.");
                }

                $ticket->tic_cancelado = 1;

                $this->tables->getTicketTable()->salvarTicket($ticket);

                $retorno['salvou'] = true;
                $retorno['mensagem'] = "Ticket cancelado com sucesso!";
                $retorno['alertType'] = "success";

            } catch (\Exception $e) {
                $retorno['salvou'] = false;
                $retorno['mensagem'] = $e->getMessage();
                $retorno['error'] = "success";
            }
        }
        
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    public function reenviarEmailAction()
    {
        $view = new ViewModel();

        $request = $this->getRequest();
        $response = $this->getResponse();

        $post = $request->getPost();

        // Se for post e ajax
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            
            $ticket = $this->tables->getTicketTable()->getTicketById($post['id']);

            if($ticket){
               $cliente = $this->tables->getClienteTable()->getClienteById($ticket->tic_cli_id);
               $produto = $this->tables->getEventoProdutoTable()->getEventoProdutoById($ticket->tic_evp_id);

               if($produto && $cliente){

                    $send_email = $this->getSendGrid();
                   
                    $evento = $this->tables->getEventoTable()->getEventoById($produto->evp_eve_id);

                    $dados_email = [
                        'cliente' => $cliente,
                        'produto' => $produto,
                        'evento' => $evento,
                        'ticket' => $ticket
                    ];

                    $layout = $send_email->montaLayout($dados_email, 'confirmacao-venda', 'default', 'default', 'default');
                    $send_email->sendEmail($layout, array($cliente->cli_email => $cliente->cli_nome), $evento->eve_nome.' - aqui está sua entrada', null, array(), array(), '', '');
                    
                    $data['mensagem'] = 'E-mail reenviado ao cliente com sucesso';
                    $data['type'] = 'success';
                    $response->setContent(json_encode($data));  
                    return $response;    
                }
            }

            $data['mensagem'] = 'Não foi possivel enviar o e-mail para o cliente.';
            $data['type'] = 'warning';
            $response->setContent(json_encode($data));  
            return $response;

        } else {
            $response->setStatusCode(404);
            return;
        }
    }

    public function getSendGrid()
    {
        return new \Email\Service\SendGrid($this->sm);
    }
}

<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use PainelAdmin\Entity\Usuario;
use PainelAdmin\Entity\UsuarioEvento;
use PainelAdmin\Filter\SenhaFilter;
use PainelAdmin\Form\UsuarioForm;
use PainelAdmin\Form\SenhaForm;

use Application\Exception\ValidationException;

class UsuarioController extends AbstractActionController
{
	private $sm;
	private $user;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        $paginasParaNaoRedirecionar = [
            'alterar-senha',
            'salvar-senha'
        ];

        if ((!$this->user || $this->user->usu_ust_id != 1) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
            return $this->redirect()->toRoute('login');
        }

    	$inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
    	$inlineScript->prependFile('/js/usuario.js');
	}

	public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';
        $status = (isset($dados->status)) ? $dados->status : '';
        $tipo = (isset($dados->tipo)) ? $dados->tipo : '';

        $usuario_tipo = $this->tables->getUsuarioTipoTable()->getUsuarioTipos();

        $usuarios = $this->tables->getUsuarioTable()->getUsuarios($pesquisar, $tipo, $status);

        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($usuarios);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

        return new ViewModel([
            'usuarios' => $paginator,
            'dados' => $dados,
            'usuario_tipo' => $usuario_tipo,
        ]);
    }

    public function cadastrarAction()
    {

        $request = $this->getRequest();

        if($request->isXmlHttpRequest() && $request->isPost()) {

        	try{
        		$response = $this->getResponse();

	            $post = $request->getPost(); 
				parse_str($post['dados'], $dados);

				$usuario_email = $this->tables->getUsuarioTable()->getUsuarioByEmail($dados['usu_email']);

				if($usuario_email){
					throw new ValidationException('Já existe um usuário com este e-mail!');
				}

	            $usuario = new Usuario();
	            $usuario->exchangeArray($dados);

	            $senha = $this->gerarSenhaAleatoria();
	            $usuario->setSenha($senha);

	            $usuario->usu_id = $this->tables->getUsuarioTable()->salvarUsuario($usuario);

	            $send_email = $this->getSendEmail();

	            $dados_email = [
	            	'nome' => $dados['usu_nome'],
	            	'senha' => $senha
	            ];

                $layout = $send_email->montaLayout($dados_email, 'novo-cadastro', 'default', 'default', 'default');
                $send_email->sendEmail($layout, array($dados['usu_email'] => $dados['usu_nome']), 'Projeto Pós PUC - Cadastro Efetuado', null, array(), array(), '', '');

	            $retorno['sucesso'] = true;
	            $retorno['path'] = '/painel/usuarios';
	            $retorno['mensagem'] = 'Usuário cadastrado com sucesso';

        	} catch (ValidationException $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = true;
                $retorno['mensagem'] = $e->getMessage();
            } catch (\Exception $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = false;
                $retorno['mensagem'] = $e->getMessage();

                if(strpos($_SERVER['HTTP_HOST'], "localhost") !== false){
                    $retorno['interno'] = true;
                }
            }

            $response->setContent(\Zend\Json\Json::encode($retorno));
            return $response;

        } else {
            $form = new UsuarioForm($this->sm, 'form-usuario');
            
            return new ViewModel([
                'form' => $form,
                'textButton' => 'Cadastrar'
            ]);
        }
    }

    public function editarAction()
    {
        $id =  $this->params()->fromRoute('id'); 
        $request = $this->getRequest();

        $usuario = $this->tables->getUsuarioTable()->getUsuarioById($id);

        if($request->isXmlHttpRequest() && $request->isPost()) {

            $response = $this->getResponse();

            $post = $request->getPost(); 
            parse_str($post['dados'], $dados);

            $usuario->exchangeArray($dados);
            $this->tables->getUsuarioTable()->salvarUsuario($usuario);

            $retorno['sucesso'] = true;
            $retorno['mensagem'] = 'Usuário editado com sucesso';

            $response->setContent(\Zend\Json\Json::encode($retorno));
            return $response;

        } else {
            $form = new UsuarioForm($this->sm, 'form-usuario');
			$form->populateForm($usuario);

            $form->setAttribute('action', '/painel/usuarios/editar/'.$id);

            $view = new ViewModel();
            $view->setTemplate('painel-admin/usuario/cadastrar');

            $view->setVariables([
                'form' => $form,
                'textButton' => 'Editar'
            ]);

            return $view;
        }
    }

    public function ativarInativarUsuarioAction()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $retorno['sucesso'] = false;
        $retorno['validacao'] = false;
        
        if($request->isXmlHttpRequest() && $request->isPost()) {
            
            try{
                $post = $request->getPost(); 
            
                $usuario = $this->tables->getUsuarioTable()->getUsuarioById($post->id);

                if(!$usuario){
                    throw new ValidationException('Usuário não encontrado!');
                }

                if($usuario->usu_ativo){
                    $this->tables->getUsuarioTable()->desativarUsuario($post->id);
                } else {
                    $this->tables->getUsuarioTable()->ativarUsuario($post->id);
                }
                
                $retorno['sucesso'] = true;
                $retorno['mensagem'] = 'Usuário editado com sucesso';
            } catch (ValidationException $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = true;
                $retorno['mensagem'] = $e->getMessage();
            } catch (\Exception $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = false;
                $retorno['mensagem'] = $e->getMessage();

                if(strpos($_SERVER['HTTP_HOST'], "localhost") !== false){
                    $retorno['interno'] = true;
                }
            }
        }

        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    public function alterarSenhaAction()
    {
        $tables = $this->sm->get('tables');

		$view = new ViewModel();

        $form_senha = new SenhaForm();

        $view->setVariables([
            'form_senha' => $form_senha,
            'usuario' => $this->user
        ]);

		return $view;
    }

    public function salvarSenhaAction()
	{
	  	$request = $this->getRequest();

		$response = $this->getResponse();

		$retorno['salvou'] = false;

        $user = $this->sm->get('Session')->offsetGet('user');

		if($request->isXmlHttpRequest() && $request->isPost() && $user) {
			try{
                $tb = $this->sm->get('tables');

				$post = $request->getPost();
				$dados = [];
				parse_str($post['dados'], $dados);

				if($dados['nova_senha'] != $dados['confirmar_senha']){
					throw new \Exception('Nova senha e confirmação devem ser iguais.');
				}
			
				$form = new SenhaForm();
				$filter = new SenhaFilter();

				$form->setInputFilter($filter->getInputFilter());
				$form->setData($dados);

				if($form->isValid()){
					$user = $this->sm->get('Session')->offsetGet('user');

					if($user->usu_senha == md5($dados['usu_senha'])){

			  			$tb->begin(); 

			  			$att_user = $tb->getUsuarioTable()->getUsuarioByEmail($user->usu_email);
			  			$att_user->setSenha($dados['nova_senha']);
			  			$tb->getUsuarioTable()->salvarUsuario($att_user);
                          
                        $tb->commit();

			  			$this->sm->get('Session')->offsetSet('user', $att_user);

			  			$retorno['mensagem'] = 'Senha atualizada com sucesso.';
			  			$retorno['salvou'] = true;
		  			} else {
		  				throw new \Exception('Senha incorreta.');
		  			}
		  		}else{
		  			throw new \Exception('Formulário inválido.');
		  		}
			} catch (\Exception $e) {
                $retorno['salvou'] = false;
                $retorno['mensagem'] = $e->getMessage();
            }

			// altera o conteudo da resposta
            $response->setContent(\Zend\Json\Json::encode($retorno));
            return $response;
	  	} else {
			$response->setStatusCode(404);
			return;
	  	} 
	}

	public function modalUsuarioEventoAction()
    {
        $view = new ViewModel();

        $request = $this->getRequest();

        $post = $request->getPost();

        // Se for post e ajax
        if ($request->isPost() && $request->isXmlHttpRequest()) {

        	$usuario = $this->tables->getUsuarioTable()->getUsuarioById($post->usuario_id);

        	$dados = $post;
        	$pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';
	        $status = (isset($dados->status)) ? $dados->status : '';

	        $eventos = $this->tables->getEventoTable()->getEventos($pesquisar, $status);

	        $usuario_eventos = $this->tables->getUsuarioEventoTable()->getUsuarioEventoByUsuario($usuario->usu_id);

	        $array_usuario_eventos = [];
	        foreach ($usuario_eventos as $usuario_evento) {
	        	$array_usuario_eventos[] = $usuario_evento->use_eve_id;
	        }

	        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($eventos);
	        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
	        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
	        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

            $view->setTerminal(true);
            $view->setVariables([
                'usuario' => $usuario,
                'eventos' => $paginator,
                'dados' => $dados,
                'usuario_eventos' => $array_usuario_eventos,
            ]);
        }

        return $view;
    }

    public function vincularDesvincularUsuarioEventoAction()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $retorno['sucesso'] = false;
        $retorno['validacao'] = false;
        
        if($request->isXmlHttpRequest() && $request->isPost()) {
            
            try{
                $post = $request->getPost(); 
            
                $evento = $this->tables->getEventoTable()->getEventoById($post->evento_id);

                if(!$evento){
                    throw new ValidationException('Evento não encontrado!');
                }

                $usuario = $this->tables->getUsuarioTable()->getUsuarioById($post->usuario_id);

                if(!$usuario){
                    throw new ValidationException('Usuário não encontrado!');
                }

                if($usuario->usu_ust_id != 3){
                	throw new ValidationException('Só é possível relacionar eventos à usuários Ponto de Venda!');
                }

                $usuario_evento = $this->tables->getUsuarioEventoTable()->getByUsuarioAndEvento($post->usuario_id, $post->evento_id);

                if(!$usuario_evento){
                    $usuario_evento = new UsuarioEvento();
                    $usuario_evento->use_usu_id = $usuario->usu_id;
                    $usuario_evento->use_eve_id = $evento->eve_id;
                    $usuario_evento->use_ativo = true;

                    $this->tables->getUsuarioEventoTable()->salvarUsuarioEvento($usuario_evento);
                }else{
                	$usuario_evento->use_ativo = false;

                	$this->tables->getUsuarioEventoTable()->salvarUsuarioEvento($usuario_evento);
                }
                
                $retorno['sucesso'] = true;
                $retorno['mensagem'] = 'Relação editada com sucesso';
            } catch (ValidationException $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = true;
                $retorno['mensagem'] = $e->getMessage();
            } catch (\Exception $e) {
                $retorno['sucesso'] = false;
                $retorno['validacao'] = false;
                $retorno['mensagem'] = $e->getMessage();

                if(strpos($_SERVER['HTTP_HOST'], "localhost") !== false){
                    $retorno['interno'] = true;
                }
            }
        }

        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

	private function gerarSenhaAleatoria()
    {
        return rand(100000, 999999);
    }

    public function getSendEmail()
    {
        return new \Email\Service\SendGrid($this->sm);
    }
}
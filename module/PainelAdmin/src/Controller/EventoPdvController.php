<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use PainelAdmin\Form\EventoForm;
use PainelAdmin\Entity\Evento;

use Application\Exception\ValidationException;

class EventoPdvController extends AbstractActionController
{
    private $env;
	private $sm;
	private $user;
    private $tables;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        $paginasParaNaoRedirecionar = [];

        if ((!$this->user || $this->user->usu_ust_id != 3) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
            return $this->redirect()->toRoute('login');
        }

        $inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
        $inlineScript->appendFile('/js/evento-pdv.js');
	}

    public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';

        $eventos = $this->tables->getEventoTable()->getEventosByPdv($this->user->usu_id, $pesquisar);

        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($eventos);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

        return new ViewModel([
            'eventos' => $paginator,
            'dados' => $dados,
        ]);
    }

    public function modalDetalhesAction()
    {
        $view = new ViewModel();

        $request = $this->getRequest();

        $post = $request->getPost();

        // Se for post e ajax
        if ($request->isPost() && $request->isXmlHttpRequest()) {

            $evento = $this->tables->getEventoTable()->getEventoById($post['id']);

            $view->setTerminal(true);
            $view->setVariables([
                'evento' => $evento,
            ]);
        }

        return $view;
    }
}

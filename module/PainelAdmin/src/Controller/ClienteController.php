<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use PainelAdmin\Form\ClienteForm;
use PainelAdmin\Entity\Cliente;

use Application\Exception\ValidationException;

class ClienteController extends AbstractActionController
{
    private $env;
	private $sm;
	private $user;
    private $tables;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        $paginasParaNaoRedirecionar = [];

        if ((!$this->user || $this->user->usu_ust_id == 2) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
            return $this->redirect()->toRoute('login');
        }

        $inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
        $inlineScript->appendFile('/js/cliente.js');
	}

    public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $pesquisar = (isset($dados->pesquisar)) ? $dados->pesquisar : '';

        $clientes = $this->tables->getClienteTable()->getClientes($pesquisar);

        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($clientes);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber((isset($dados->pagina)) ? $dados->pagina : 1);
        $paginator->setItemCountPerPage((isset($dados->numero_itens)) ? $dados->numero_itens : 20);

        return new ViewModel([
            'clientes' => $paginator,
            'dados' => $dados,
        ]);
    }

    public function editarAction()
    {
        $id =  $this->params()->fromRoute('id'); 
        $request = $this->getRequest();

        $cliente = $this->tables->getClienteTable()->getClienteById($id);

        if($request->isXmlHttpRequest() && $request->isPost()) {

            $response = $this->getResponse();

            $post = $request->getPost(); 
            parse_str($post['dados'], $dados);

            $cliente->exchangeArray($dados);
            $this->tables->getClienteTable()->salvarCliente($cliente);

            $retorno['sucesso'] = true;
            $retorno['mensagem'] = 'Cliente editado com sucesso';

            $response->setContent(\Zend\Json\Json::encode($retorno));
            return $response;

        } else {
            $form = new ClienteForm('form-cliente', $this->sm);
			$form->populateForm($cliente);

            $form->setAttribute('action', '/painel/clientes/editar/'.$id);

            $view = new ViewModel();
            $view->setTemplate('painel-admin/cliente/cadastrar');

            $view->setVariables([
                'form' => $form,
                'textButton' => 'Editar'
            ]);

            return $view;
        }
    }
}

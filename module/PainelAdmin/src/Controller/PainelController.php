<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class PainelController extends AbstractActionController
{
	private $sm;
	private $user;
    private $config;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');
        $this->config = $this->sm->get('config');

        $paginasParaNaoRedirecionar = [
            'login',
            'logout',
            'recuperar-senha',
            'index',
        ];

        if ((!$this->user) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
        	return $this->redirect()->toRoute('login');
        }
	}

    public function indexAction()
    {
        if(!$this->user){
            return $this->redirect()->toRoute('login');
        }else{

            if($this->user->usu_ust_id == 1){
                return $this->indexAdmin();
            }else if($this->user->usu_ust_id == 3){
                return $this->indexPdv();
            }

            return new ViewModel();
        }
    }

    public function indexAdmin(){

        $eventos = $this->tables->getEventoTable()->getEventos('', 1);

        $clientes = $this->tables->getClienteTable()->getClientes('');

        $tickets_mes = $this->tables->getTicketTable()->getTicketsVendidosMes();

        $total_vendas = 0;
        foreach ($tickets_mes as $key => $value) {
            $total_vendas += $value->count;
        }

        $origem_vendas_mes = $this->tables->getTicketTable()->getOrigemVendasMes();

        $view = new ViewModel([
            'clientes' => $clientes,
            'eventos_ativos' => $eventos,
            'tickets_mes' => $tickets_mes,
            'total_vendas' => $total_vendas,
            'origem_vendas_mes' => $origem_vendas_mes,
        ]);

        $view->setTemplate('painel-admin/painel/index-admin');

        return $view;
    }

    public function indexPdv(){

        $eventos = $this->tables->getEventoTable()->getEventosByPdv($this->user->usu_id);

        $tickets = $this->tables->getTicketTable()->getTicketsByPDV($this->user->usu_id);

        $view = new ViewModel([
            'eventos' => $eventos,
            'tickets' => $tickets
        ]);

        $view->setTemplate('painel-admin/painel/index-pdv');

        return $view;
    }

    public function loginAction()
    {
        $request = $this->getRequest();
      
        $response = $this->getResponse();

        if($request->isXmlHttpRequest() && $request->isPost()) {          
            $post = $request->getPost();

            $data = '';
            parse_str($post['dados'], $data);

            $data['auth'] = $this->sm->get('auth')->autenticar($data);
            $data['admin'] = false;
            
            if($data['auth']){
                $user = $this->sm->get('Session')->offsetGet('user');

                if($user && isset($user->usu_ust_id) && $user->usu_ust_id == 2){
                    $data['path'] = '/painel/leitor-qr';
                }else{
                    $data['path'] = '/painel';
                }
            } else {
                $data['mensagem'] = 'Usuário ou senha não conferem';
            }

            $response->setContent(json_encode($data));  
            return $response;
        } else {
            // Busca usuário na sessão
            $user = $this->sm->get('Session')->offsetGet('user');

            // Redireciona para página inicial do usuário logado
            if($user){
                if(isset($user->usu_ust_id) && $user->usu_ust_id == 2){
                    return $this->redirect()->toRoute('leitor-qr');
                }else{
                    return $this->redirect()->toRoute('painel');
                }
            }

            // Seta layout de login
            $this->layout()->setTemplate('layout/no-layout');

            // Caso não teve usuário logado, joga pra página de login
            return new ViewModel([]);
        }
    }

    public function logoutAction()
    {
        $logout = $this->sm->get('auth')->logout();

        if($logout){
            return $this->redirect()->toRoute('login');
        } else {
            $this->getResponse()->setStatusCode(404);
            return;
        }
    }

    public function recuperarSenhaAction()
    {
        $request = $this->getRequest();
      
        $response = $this->getResponse();

        if($request->isXmlHttpRequest() && $request->isPost()) {          
            $post = $request->getPost();

            $tb = $this->sm->get('tables');

            $usuario = $tb->getUsuarioTable()->getUsuarioByEmail($post['email']);

            if($usuario){

                $senha = rand(100000, 999999);
                
                $usuario->setSenha($senha);
                $tb->getUsuarioTable()->salvarUsuario($usuario);

                $send_email = $this->getSendEmail();
                $layout = $send_email->montaLayout(
                    [
                        'nome' => $usuario->usu_nome,
                        'senha' => $senha
                    ],
                    'recuperar-senha',
                    'default',
                    'default',
                    'default'
                );
                $send_email->sendEmail($layout, array($usuario->usu_email => $usuario->usu_nome), 'Nova senha de acesso', null, array(), array(), '', '');

                $data['mensagem'] = 'Um e-mail foi enviado para você com sua nova senha de acesso.';
                $data['type'] = 'success';
            } else {
                $data['mensagem'] = 'E-mail não registrado no sistema';
                $data['type'] = 'warning';
            }

            $response->setContent(json_encode($data));  
            return $response;
        } else {
            $response->setStatusCode(404);
            return;
        }
    }

    public function carregaSelectProdutosAction()
    {
        $tables = $this->sm->get('tables');

        // instancia o objeto da visao
        $view = new ViewModel();
    
        // captura requisicao
        $request = $this->getRequest();
    
        // forca a saida sem template para uma requisicao ajax
        $view->setTerminal($request->isXmlHttpRequest());
    
        // verifica se eh um requisicao ajax
        if($request->isXmlHttpRequest()){

            // recupera os dados do post
            $post = $request->getPost();

            if($post->evento != '' && $post->evento != null){
                $dados = $tables->getEventoProdutoTable()->getEventoProdutos('', $post->evento);
            }else{
                $dados = array();
            }

            $view->setTerminal($request->isXmlHttpRequest());

            $view->setVariables(array(
                'dados' => $dados,
            ));
        }
    
        return $view;
    }

    /*public function testeEmailAction()
    {
        $dados_email = [
            'nome' => 'Nome Teste',
            'senha' => 'Senha Teste',
        ];

        $send_email = $this->getSendEmail();

        $layout = $send_email->montaLayout($dados_email, 'recuperar-senha', 'default', 'default', 'default');

        $assunto = 'Recuperação de Senha Projeto Pós PUC';

        var_dump($send_email->sendEmail($layout, array('david.alves.dutra@hotmail.com' => 'DvDutra'), $assunto));

        exit;
    }*/

    public function getSendEmail()
    {
        return new \Email\Service\SendGrid($this->sm);
    }
}

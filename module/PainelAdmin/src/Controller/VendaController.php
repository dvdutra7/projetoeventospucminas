<?php
namespace PainelAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use PainelAdmin\Entity\Ticket;
use PainelAdmin\Entity\Cliente;

use Application\Exception\ValidationException;

class VendaController extends AbstractActionController
{
    private $env;
	private $sm;
	private $user;
    private $tables;

	protected function attachDefaultListeners()
	{
		parent::attachDefaultListeners();
		$events = $this->getEventManager();
		$this->events->attach('dispatch', array($this, 'preDispatch'), 100); 
	}

	public function preDispatch (MvcEvent $e)
	{
		$this->sm = $e->getApplication()->getServiceManager();
        $this->user = $this->sm->get('Session')->offsetGet('user');
        $this->tables = $this->sm->get('tables');

        $paginasParaNaoRedirecionar = [];

        if ((!$this->user || $this->user->usu_ust_id != 3) && !in_array($this->params('action'), $paginasParaNaoRedirecionar)) {
            return $this->redirect()->toRoute('login');
        }

        $inlineScript = $this->sm->get('ViewHelperManager')->get('inlineScript');
        $inlineScript->appendFile('/js/venda.js');
	}

    public function indexAction()
    {
        $request = $this->getRequest();
        $dados = $request->getQuery();

        $produto = (isset($dados->produto)) ? $dados->produto : '';
        $evento = (isset($dados->evento)) ? $dados->evento : '';

        $eventos = $this->tables->getEventoTable()->getEventosByPdv($this->user->usu_id);

        if($evento != ''){
            $produtos = $this->tables->getEventoProdutoTable()->getEventoProdutos('', $evento);
        }else{
            $produtos = [];
        }

        $form = new \PainelAdmin\Form\ClienteForm('form-ticket');
        $form->setAttribute('action', '/painel/vendas/efetuar-venda');
        
        return new ViewModel([
            'form' => $form,
            'eventos' => $eventos,
            'produtos' => $produtos,
            'dados' => $dados,
        ]);
    }

    public function efetuarVendaAction()
    {
        $request = $this->getRequest();

        if($request->isXmlHttpRequest() && $request->isPost()) {

            try{
                $this->tables->begin();

                $response = $this->getResponse();

                $post = $request->getPost(); 
                parse_str($post['dados'], $dados);

                $cliente = $this->tables->getClienteTable()->getClienteByEmail($dados['cli_email']);

                if(!$cliente){
                    $cliente = new Cliente();
                    $cliente->exchangeArray($dados);

                    $cliente->cli_id = $this->tables->getClienteTable()->salvarCliente($cliente);
                }

                $ticket = new Ticket();
                $ticket->tic_cli_id = $cliente->cli_id;
                $ticket->tic_evp_id = $dados['produto'];
                $ticket->tic_usu_id = $this->user->usu_id;

                $ticket->tic_id = $this->tables->getTicketTable()->salvarTicket($ticket);

                $produto = $this->tables->getEventoProdutoTable()->getEventoProdutoById($dados['produto']);
                $evento = $this->tables->getEventoTable()->getEventoById($dados['venda_evento']);

                $send_email = new \Email\Service\SendGrid($this->sm);

                $dados_email = [
                    'cliente' => $cliente,
                    'produto' => $produto,
                    'evento' => $evento,
                    'ticket' => $ticket
                ];

                $layout = $send_email->montaLayout($dados_email, 'confirmacao-venda', 'default', 'default', 'default');
                $send_email->sendEmail($layout, array($dados['cli_email'] => $dados['cli_nome']), $evento->eve_nome.' - Compra efetuada com sucesso.', null, array(), array(), '', '');

                $retorno['sucesso'] = true;
                $retorno['path'] = '/painel/vendas';
                $retorno['mensagem'] = 'Venda efetuada com sucesso';

                $this->tables->commit();

            } catch (ValidationException $e) {
                $this->tables->rollback();

                $retorno['sucesso'] = false;
                $retorno['validacao'] = true;
                $retorno['mensagem'] = $e->getMessage();
            } catch (\Exception $e) {
                $this->tables->rollback();

                $retorno['sucesso'] = false;
                $retorno['validacao'] = false;
                $retorno['mensagem'] = $e->getMessage();

                if(strpos($_SERVER['HTTP_HOST'], "localhost") !== false){
                    $retorno['interno'] = true;
                }
            }

            $response->setContent(\Zend\Json\Json::encode($retorno));
            return $response;

        } else {
            $this->getResponse()->setStatusCode(404);
        return;
        }
    }
}

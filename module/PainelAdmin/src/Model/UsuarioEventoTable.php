<?php

namespace PainelAdmin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\SequenceFeature;

use PainelAdmin\Entity\UsuarioEvento;

class UsuarioEventoTable 
{
    private $adapter;
    private $tableGateway;
    private $table = 'usuario_evento';
    private $sequence = 'use_id';

    public function __construct($container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new UsuarioEvento());
        $featureSet = new FeatureSet();
        $featureSet->addFeature(new SequenceFeature('use_id', $this->sequence));
        $this->tableGateway = new TableGateway($this->table, $dbAdapter, $featureSet, $resultSetPrototype);   

        $this->adapter = $dbAdapter;
    }

    public function getUsuarioEventoById($id)
    {
        return $this->tableGateway->select(['use_id' => $id])->current();
    }

    public function getByUsuarioAndEvento($usuario_id, $evento_id)
    {
        return $this->tableGateway->select(['use_usu_id' => $usuario_id, 'use_eve_id' => $evento_id])->current();
    }

    public function getUsuarioEventoByUsuario($usuario)
    {
        $result = $this->tableGateway->select(function (Select $select) use ($usuario) {
            $select->where(['use_usu_id' => $usuario]);
        });

        $result->buffer();

        return $result;
    }

    public function salvarUsuarioEvento(UsuarioEvento $usuario_evento)
    {
        $data = [
            'use_usu_id' => $usuario_evento->use_usu_id,
            'use_eve_id' => $usuario_evento->use_eve_id,
            'use_ativo' => $usuario_evento->use_ativo,
        ];

        $id = (int) $usuario_evento->use_id;
        try {

            if($id === 0){
                $this->tableGateway->insert($data);
                return $this->adapter->getDriver()->getLastGeneratedValue();
            } else {
                if($this->getUsuarioEventoById($id)){
                    $this->tableGateway->update($data, ['use_id' => $id]);
                }
            }
        } catch (Exception $e) {
            echo 'Exceção capturada: ',  $e->getMessage(), "\n";
        }
            
    }
}
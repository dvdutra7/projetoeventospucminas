<?php

namespace PainelAdmin\Model;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\SequenceFeature;

use PainelAdmin\Entity\Usuario;

class UsuarioTable 
{
    private $tableGateway;
    private $table = 'usuario';
    private $sequence = 'usu_id';

    public function __construct($container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Usuario());
        $featureSet = new FeatureSet();
        $featureSet->addFeature(new SequenceFeature('usu_id', $this->sequence));
        $this->tableGateway = new TableGateway($this->table, $dbAdapter, $featureSet, $resultSetPrototype);   
    }

    public function getUsuarioById($id)
    {
        return $this->tableGateway->select(['usu_id' => $id])->current();
    }

    public function getUsuarioByEmail($email)
    {
        return  $this->tableGateway->select(['usu_email' => $email])->current();
    }

    public function getUsuarios($pesquisar = '', $tipo = '', $status = '')
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pesquisar, $tipo, $status) {
            $select->join('usuario_tipo', 'ust_id = usu_ust_id', ['ust_nome']);
            if ($pesquisar != ''){
                $select->where("(lower(usu_nome) LIKE '%".strtolower($pesquisar)."%' OR lower(usu_email) LIKE '%".strtolower($pesquisar)."%')");
            }

            if($tipo != ''){
                $select->where(['usu_ust_id' => $tipo]);
            }

            if($status != ''){
                $select->where(['usu_ativo' => $status]);
            }
        });

        $result->buffer();

        return $result;
    }

    public function salvarUsuario(Usuario $usuario)
    {
        $data = [
            'usu_nome' => $usuario->usu_nome,
            'usu_email' => $usuario->usu_email,
            'usu_senha' => $usuario->usu_senha,
            'usu_ust_id' => $usuario->usu_ust_id,
        ];

        $id = (int) $usuario->usu_id;
        try {

            if($id === 0){
                $this->tableGateway->insert($data);
                return $this->getUsuarioByEmail($usuario->usu_email)->usu_id;
            } else {
                if($this->getUsuarioById($id)){
                    $this->tableGateway->update($data, ['usu_id' => $id]);
                }
            }
        } catch (Exception $e) {
            echo 'Exceção capturada: ',  $e->getMessage(), "\n";
        }    
    }

    public function desativarUsuario($usuario_id)
    {
        $this->tableGateway->update(['usu_ativo' => false], ['usu_id' => $usuario_id]);
    }

    public function ativarUsuario($usuario_id)
    {
        $this->tableGateway->update(['usu_ativo' => true], ['usu_id' => $usuario_id]);
    }
}
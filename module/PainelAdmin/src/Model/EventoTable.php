<?php

namespace PainelAdmin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\SequenceFeature;

use PainelAdmin\Entity\Evento;

class EventoTable 
{
    private $adapter;
    private $tableGateway;
    private $table = 'evento';
    private $sequence = 'eve_id';

    public function __construct($container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Evento());
        $featureSet = new FeatureSet();
        $featureSet->addFeature(new SequenceFeature('eve_id', $this->sequence));
        $this->tableGateway = new TableGateway($this->table, $dbAdapter, $featureSet, $resultSetPrototype);   

        $this->adapter = $dbAdapter;
    }

    public function getEventoById($id)
    {
        return $this->tableGateway->select(['eve_id' => $id])->current();
    }

    public function getEventos($pesquisar = '', $status = '')
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pesquisar, $status) {

            if($status != ''){
                $select->where(['eve_ativo' => $status]);
            }

            if ($pesquisar != ''){
                $select->where("(lower(eve_nome) LIKE '%".strtolower($pesquisar)."%')");
            }
            $select->order('eve_id DESC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function getEventosByPdv($pdv, $pesquisar = '')
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pdv, $pesquisar) {

            $select->join('usuario_evento', 'use_eve_id = eve_id');
            $select->where(["use_usu_id" => $pdv, "use_ativo is true", "eve_ativo is true"]);

            if ($pesquisar != ''){
                $select->where("(lower(eve_nome) LIKE '%".strtolower($pesquisar)."%')");
            }
            $select->order('eve_id DESC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function salvarEvento(Evento $evento)
    {
        $data = [
            'eve_nome' => $evento->eve_nome,
            'eve_local' => $evento->eve_local,
            'eve_cidade' => $evento->eve_cidade,
        ];

        $id = (int) $evento->eve_id;
        try {

            if($id === 0){
                $this->tableGateway->insert($data);
                return $this->adapter->getDriver()->getLastGeneratedValue();
            } else {
                if($this->getEventoById($id)){
                    $this->tableGateway->update($data, ['eve_id' => $id]);
                }
            }
        } catch (Exception $e) {
            echo 'Exceção capturada: ',  $e->getMessage(), "\n";
        }
            
    }

    public function desativarEvento($evento_id)
    {
        $this->tableGateway->update(['eve_ativo' => false], ['eve_id' => $evento_id]);
    }

    public function ativarEvento($evento_id)
    {
        $this->tableGateway->update(['eve_ativo' => true], ['eve_id' => $evento_id]);
    }
}
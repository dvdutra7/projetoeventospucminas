<?php

namespace PainelAdmin\Model;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\SequenceFeature;

use PainelAdmin\Entity\UsuarioTipo;

class UsuarioTipoTable 
{
    private $tableGateway;
    private $table = 'usuario_tipo';
    private $sequence = 'ust_id';

    public function __construct($container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new UsuarioTipo());
        $featureSet = new FeatureSet();
        $featureSet->addFeature(new SequenceFeature('ust_id', $this->sequence));
        $this->tableGateway = new TableGateway($this->table, $dbAdapter, $featureSet, $resultSetPrototype);   
    }

    public function getUsuarioTipos()
    {
        $result = $this->tableGateway->select(function (Select $select){
            $select->order(['ust_id ASC']);
        });

        $result->buffer();

        return $result;
    }
}
<?php

namespace PainelAdmin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\SequenceFeature;

use PainelAdmin\Entity\EventoProduto;

class EventoProdutoTable 
{
    private $adapter;
    private $tableGateway;
    private $table = 'evento_produto';
    private $sequence = 'evp_id';

    public function __construct($container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new EventoProduto());
        $featureSet = new FeatureSet();
        $featureSet->addFeature(new SequenceFeature('evp_id', $this->sequence));
        $this->tableGateway = new TableGateway($this->table, $dbAdapter, $featureSet, $resultSetPrototype);   

        $this->adapter = $dbAdapter;
    }

    public function getEventoProdutoById($id)
    {
        return $this->tableGateway->select(['evp_id' => $id])->current();
    }

    public function getEventoProdutos($pesquisar = '', $evento = '')
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pesquisar, $evento) {
            $select->columns(['*']);
            $select->join('evento', 'evp_eve_id = eve_id', ['*']);

            if($pesquisar != ''){
                $select->where("(lower(evp_nome) LIKE '%".strtolower($pesquisar)."%')");
            }

            if($evento != ''){
                $select->where(["evp_eve_id" => $evento]);
            }

            $select->where(["evp_excluido" => false]);

            $select->order("evp_id ASC");
        });

        $result->buffer();

        return $result;
    }

    public function salvarEventoProduto(EventoProduto $evento_produto)
    {
        $data = [
            'evp_eve_id' => $evento_produto->evp_eve_id,
            'evp_nome' => $evento_produto->evp_nome,
            'evp_preco' => $evento_produto->evp_preco,
        ];

        $id = (int) $evento_produto->evp_id;
        try {

            if($id === 0){
                $this->tableGateway->insert($data);
                return $this->adapter->getDriver()->getLastGeneratedValue();
            } else {
                if($this->getEventoProdutoById($id)){
                    $this->tableGateway->update($data, ['evp_id' => $id]);
                }
            }
        } catch (Exception $e) {
            echo 'Exceção capturada: ',  $e->getMessage(), "\n";
        }
            
    }

    public function excluirEventoProduto($evento_produto_id)
    {
        $this->tableGateway->update(['evp_excluido' => true], ['evp_id' => $evento_produto_id]);
    }
}
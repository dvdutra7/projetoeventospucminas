<?php

namespace PainelAdmin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\SequenceFeature;

use PainelAdmin\Entity\Cliente;

class ClienteTable 
{
    private $adapter;
    private $tableGateway;
    private $table = 'cliente';
    private $sequence = 'cli_id';

    public function __construct($container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Cliente());
        $featureSet = new FeatureSet();
        $featureSet->addFeature(new SequenceFeature('cli_id', $this->sequence));
        $this->tableGateway = new TableGateway($this->table, $dbAdapter, $featureSet, $resultSetPrototype);   

        $this->adapter = $dbAdapter;
    }

    public function getClienteById($id)
    {
        return $this->tableGateway->select(['cli_id' => $id])->current();
    }

    public function getClienteByEmail($email)
    {
        return $this->tableGateway->select(['cli_email' => $email])->current();
    }

    public function getClientes($pesquisar = '')
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pesquisar) {
            if ($pesquisar != ''){
                $select->where("(lower(cli_nome) LIKE '%".strtolower($pesquisar)."%' OR (lower(cli_email) LIKE '%".strtolower($pesquisar)."%')");
            }

            $select->order('cli_nome ASC');
        });

        $result->buffer();

        return $result;
    }

    public function salvarCliente(Cliente $cliente)
    {
        $data = [
            'cli_nome' => $cliente->cli_nome,
            'cli_email' => $cliente->cli_email,
            'cli_cpf' => $cliente->cli_cpf,
            'cli_ddd' => $cliente->cli_ddd,
            'cli_telefone' => $cliente->cli_telefone,
        ];

        $id = (int) $cliente->cli_id;
        try {

            if($id === 0){
                $this->tableGateway->insert($data);
                return $this->adapter->getDriver()->getLastGeneratedValue();
            } else {
                if($this->getClienteById($id)){
                    $this->tableGateway->update($data, ['cli_id' => $id]);
                }
            }
        } catch (Exception $e) {
            echo 'Exceção capturada: ',  $e->getMessage(), "\n";
        }
            
    }

    public function desativarCliente($cliente_id)
    {
        $this->tableGateway->update(['cli_ativo' => false], ['cli_id' => $cliente_id]);
    }

    public function ativarCliente($cliente_id)
    {
        $this->tableGateway->update(['cli_ativo' => true], ['cli_id' => $cliente_id]);
    }
}
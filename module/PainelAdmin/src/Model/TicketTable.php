<?php

namespace PainelAdmin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\SequenceFeature;

use PainelAdmin\Entity\Ticket;

class TicketTable 
{
    private $adapter;
    private $tableGateway;
    private $table = 'ticket';
    private $sequence = 'tic_id';

    public function __construct($container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Ticket());
        $featureSet = new FeatureSet();
        $featureSet->addFeature(new SequenceFeature('tic_id', $this->sequence));
        $this->tableGateway = new TableGateway($this->table, $dbAdapter, $featureSet, $resultSetPrototype);   

        $this->adapter = $dbAdapter;
    }

    public function getTicketById($id)
    {
        return $this->tableGateway->select(['tic_id' => $id])->current();
    }

    public function getTickets($pesquisar = '', $status = '', $evento = '', $produto = '')
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pesquisar, $status, $evento, $produto) {

            $select->join('cliente', 'tic_cli_id = cli_id');
            $select->join('evento_produto', 'tic_evp_id = evp_id');
            $select->join('evento', 'evp_eve_id = eve_id');

            if($status != ''){
                $select->where(['tic_cancelado' => $status]);
            }

            if ($pesquisar != ''){
                $select->where("(lower(cli_nome) LIKE '%".strtolower($pesquisar)."%')");
            }

            if($produto != ''){
                $select->where(['tic_evp_id' => $produto]);
            }

            if($evento != ''){
                $select->where(['eve_id' => $evento]);
            }

            $select->order('tic_id DESC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function getTicketsByPdv($pdv)
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pdv) {

            $select->where(["tic_usu_id" => $pdv, "tic_cancelado is false"]);

            $select->order('tic_id DESC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function getTicketCompletoById($id)
    {
        $result = $this->tableGateway->select(function (Select $select) use ($id) {
            $select->join('cliente', 'tic_cli_id = cli_id', ['*'], 'left');
            $select->join('evento_produto', 'tic_evp_id = evp_id');
            $select->join('evento', 'evp_eve_id = eve_id', ['eve_nome'], 'left');
            $select->join(['pdv' => 'usuario'], 'tic_usu_id = pdv.usu_id', ['pdv_nome' => 'usu_nome'], 'left');
            $select->join(['portaria' => 'usuario'], 'tic_liberado_por = portaria.usu_id', ['liberado_por' => 'usu_nome'], 'left');
            $select->where(['tic_id' => $id]);
        });

        return $result->current();
    }

    public function getTicketsVendidosMes()
    {
        $result = $this->tableGateway->select(function (Select $select){
            $select->columns(['count' => new \Zend\Db\Sql\Expression('COUNT(tic_id)')]);
            $select->join('evento_produto', 'tic_evp_id = evp_id', ['evp_nome']);
            $select->join('evento', 'evp_eve_id = eve_id', ['eve_nome']);
            $select->where(["tic_cancelado is false", "MONTH(tic_data_cadastro) = MONTH(CURRENT_DATE)"]);
            $select->group(['evp_nome', 'eve_nome']);

            $select->order('tic_id DESC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function getOrigemVendasMes()
    {
        $result = $this->tableGateway->select(function (Select $select){
            $select->columns(['count' => new \Zend\Db\Sql\Expression('COUNT(tic_id)')]);
            $select->join('usuario', 'tic_usu_id = usu_id', ['usu_nome']);
            $select->where(["tic_cancelado is false", "MONTH(tic_data_cadastro) = MONTH(CURRENT_DATE)"]);
            $select->group(['usu_nome']);

            $select->order('tic_id DESC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function getVendasTotaisPdv($pesquisar = '', $status = '', $data_inicio = '', $data_fim = '')
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pesquisar, $status, $data_inicio, $data_fim) {
            $select->columns([
                new \Zend\Db\Sql\Expression('SUM(CASE WHEN tic_cancelado = 0 THEN 1 ELSE 0 END) AS totalAtivos'),
                new \Zend\Db\Sql\Expression('SUM(CASE WHEN tic_cancelado = 1 THEN 1 ELSE 0 END) AS totalCancelado'),
            ]);
            $select->join('usuario', 'usu_id = tic_usu_id', ['usu_nome', 'usu_id', 'usu_ativo']);
            $select->join('evento_produto', 'evp_id = tic_evp_id', ['soma' => new \Zend\Db\Sql\Expression('SUM(CASE WHEN tic_cancelado = 0 THEN evp_preco ELSE 0 END)')]);

            if ($pesquisar != ''){
                $select->where("(lower(usuario.usu_nome) LIKE '%".strtolower($pesquisar)."%')");
            }

            if($status != ''){
                $select->where(['usu_ativo' => $status]);
            }

            if ($data_inicio != ''){
                $select->where(['tic_data_cadastro >= "'.$data_inicio.'"']);
            }

            if ($data_fim != ''){
                $select->where(['tic_data_cadastro <= "'.$data_fim.'"']);
            }

            $select->group(['usu_nome']);
            $select->order('usu_nome ASC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function getVendasByPdv($pdv_id, $pesquisar = '', $data_inicio = '', $data_fim = '', $evento = '')
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pdv_id, $pesquisar, $data_inicio, $data_fim, $evento) {
            $select->columns([
                new \Zend\Db\Sql\Expression('SUM(CASE WHEN tic_cancelado = 0 THEN 1 ELSE 0 END) AS totalAtivos'),
                new \Zend\Db\Sql\Expression('SUM(CASE WHEN tic_cancelado = 1 THEN 1 ELSE 0 END) AS totalCancelado'),
            ]);
            $select->join('usuario', 'usu_id = tic_usu_id');
            $select->join('evento_produto', 'evp_id = tic_evp_id', ['soma' => new \Zend\Db\Sql\Expression('SUM(CASE WHEN tic_cancelado = 0 THEN evp_preco ELSE 0 END)'), 'evp_nome', 'evp_id']);
            $select->join('evento', 'eve_id = evp_eve_id', ['eve_nome']);

            if ($pesquisar != ''){
                $select->where("(lower(evento.eve_nome) LIKE '%".strtolower($pesquisar)."%' OR lower(evento_produto.evp_nome) LIKE '%".strtolower($pesquisar)."%')");
            }

            if ($data_inicio != ''){
                $select->where(['tic_data_cadastro >= "'.$data_inicio.'"']);
            }

            if ($data_fim != ''){
                $select->where(['tic_data_cadastro <= "'.$data_fim.'"']);
            }

            if($evento != ''){
                $select->where(['eve_id' => $evento]);
            }

            $select->where(['tic_usu_id ' => $pdv_id]);

            $select->group(['eve_nome', 'evp_nome', 'evp_id']);
            $select->order('eve_nome ASC, evp_id ASC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function getVendasByPdvAndProduto($pdv_id, $evp_id)
    {
        $result = $this->tableGateway->select(function (Select $select) use ($pdv_id, $evp_id) {
            $select->join('cliente', 'cli_id = tic_cli_id');

            $select->where(['tic_usu_id ' => $pdv_id, 'tic_evp_id' => $evp_id]);

            $select->order('tic_id ASC');
        });

        //echo $result->getDataSource()->getResource()->queryString; exit;

        $result->buffer();

        return $result;
    }

    public function salvarTicket(Ticket $ticket)
    {
        $data = [
            'tic_cli_id' => $ticket->tic_cli_id,
            'tic_evp_id' => $ticket->tic_evp_id,
            'tic_usu_id' => $ticket->tic_usu_id,
            'tic_cancelado' => ($ticket->tic_cancelado) ? $ticket->tic_cancelado : false,
            'tic_liberado_por' => $ticket->tic_liberado_por,
            'tic_data_entrada' => $ticket->tic_data_entrada,
        ];

        $id = (int) $ticket->tic_id;
        try {

            if($id === 0){
                $this->tableGateway->insert($data);
                return $this->adapter->getDriver()->getLastGeneratedValue();
            } else {
                if($this->getTicketById($id)){
                    $this->tableGateway->update($data, ['tic_id' => $id]);
                }
            }
        } catch (Exception $e) {
            echo 'Exceção capturada: ',  $e->getMessage(), "\n";
        }
            
    }

    public function cancelarTicket($ticket_id)
    {
        $this->tableGateway->update(['tic_cancelado' => true], ['tic_id' => $ticket_id]);
    }
}
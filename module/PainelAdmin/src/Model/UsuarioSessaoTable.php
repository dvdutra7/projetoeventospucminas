<?php

namespace PainelAdmin\Model;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\SequenceFeature;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

use PainelAdmin\Entity\UsuarioSessao;

class UsuarioSessaoTable 
{
    private $tableGateway;
    private $table = 'usuario_sessao';
    private $sequence = 'uss_id';

    public function __construct($container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new UsuarioSessao());
        $featureSet = new FeatureSet();
        $featureSet->addFeature(new SequenceFeature('uss_id', $this->sequence));
        $this->tableGateway = new TableGateway($this->table, $dbAdapter, $featureSet, $resultSetPrototype);   
    }

    public function getSessaoById($id)
    {
        return $this->tableGateway->select(['uss_id' => $id])->current();
    }

    public function getSessaoByHash($hash)
    {
        $result = $this->tableGateway->select(function (Select $select) use ($hash) {
            $now = date('Y-m-d H:i:s');
            $select->where(['uss_hash' => $hash]);
            $select->where("uss_data_expiracao > '".$now."'");
        });

        return $result->current();
    }

    public function getSessaoSalva($usuario, $hash)
    {
        $result = $this->tableGateway->select(function (Select $select) use ($usuario, $hash) {
            $now = date('Y-m-d H:i:s');
            $select->where(['uss_hash' => $hash, 'uss_usu_id' => $usuario]);
        });

        return $result->current();
    }

    public function salvarSessao(UsuarioSessao $sessao)
    {
        $data = [
            'uss_hash' => $sessao->uss_hash,
            'uss_usu_id' => $sessao->uss_usu_id,
            'uss_data_expiracao' => $sessao->uss_data_expiracao,
        ];

        $id = (int) $sessao->uss_id;
        try {

            if($id === 0){
                $this->tableGateway->insert($data);
            } else {
                if($this->getSessaoById($id)){
                    $this->tableGateway->update($data, ['uss_id' => $id]);
                }
            }
        } catch (Exception $e) {
            echo 'Exceção capturada: ',  $e->getMessage(), "\n";
        }
            
    }

    public function deleteSessao($usuario, $hash)
    {
        $this->tableGateway->delete(['uss_hash' => $hash, 'uss_usu_id' => $usuario]);
    }
}
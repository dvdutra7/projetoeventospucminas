<?php
namespace PainelAdmin;

use Zend\Mvc\MvcEvent;
use Detection\MobileDetect as MobileDetector;

class Module
{
    const VERSION = '3.1.3';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap($container)
    {
        $this->verificaLogado($container);

        $this->defineVariaveisComuns($container);
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'auth' => function($container) {
                    return new Service\Autenticacao($container);
                },
                'reset-password' => function($container) {
                    return new Service\PasswordReset($container);
                },
            ],
        ];
    }

    public function verificaLogado($e)
    {
        $application = $e->getApplication();

        $sm = $application->getServiceManager();

        $usuario_session = $sm->get('Session')->offsetGet('user');

        $tables = $sm->get('tables');

        // Obtém os dados do suposto usuário
        if($usuario_session == null && isset($_COOKIE['cliente_logado']) && isset($_COOKIE['manter_logado'])){
            $cookie_id = base64_decode($_COOKIE['cliente_logado']);
            $aux = explode('-', $cookie_id);

            // Se o cookie não estiver no formato adequado
            if(count($aux) != 2 || $aux[1] != 'tol'){
                // Remove os cookies
                setcookie('manter_logado', '', time() - 3600, '/');
                setcookie('cliente_logado', '', time() - 3600, '/');
            } else {
                $cookie_usu_id = $aux[0];

                $usuario_sessao = $tables->getUsuarioSessaoTable()->getSessaoSalva($cookie_usu_id, $_COOKIE['manter_logado']);

                if($usuario_sessao){

                    $sm->get('auth')->revalidarSessao($usuario_sessao->uss_usu_id);

                // Caso não seja válido para gerar um login do cliente, limpa os cookies para não fazer buscas nas próximas navegações
                }else{
                    setcookie('manter_logado', '', time() - 3600, '/');
                    setcookie('cliente_logado', '', time() - 3600, '/');
                }
            }
        }
    }

    public function defineVariaveisComuns($e) 
    {
        $application = $e->getApplication();

        $sm = $application->getServiceManager();

        $usuario_session = $sm->get('Session')->offsetGet('user');

        $mobileDetect =  new MobileDetector();

        if($usuario_session){
            $viewModel = $application->getMvcEvent()->getViewModel();

            $viewModel->setVariables([
                'usuario' => $usuario_session,
                'is_mobile' => $mobileDetect->isMobile(),
            ]);
        }

    }
}

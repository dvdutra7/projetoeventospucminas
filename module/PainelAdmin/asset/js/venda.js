function carregaSelectProdutos(){
    var evento = $('select[name=venda_evento]').find(":selected").val();

    $(".lista-produtos").load('/painel/carrega-select-produtos', {evento:evento, preco:true}, function () {
    });
}

$(document).on('submit', '#form-ticket', function (e) {
    e.preventDefault();
    formPadrao('#form-ticket');
});

$('.cpf-mask').mask('000.000.000-00');
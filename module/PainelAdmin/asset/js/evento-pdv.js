function detalhesEvento(id){
    loading('show');

    $("#modal").load('/painel/eventos-pdv/modal-detalhes', {id}, function (responseText, textStatus) {
        if(textStatus != 'error'){
            $("#modal").modal('show');
            loading('hidden');
        }else{
            erroPadrao();
        }  
    });
}
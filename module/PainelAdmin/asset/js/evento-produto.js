$(document).ready(function(){
    $("#preco").maskMoney();
});

$(document).on('submit', '#form-evento-produto', function (e) {
    e.preventDefault();
    formPadrao('#form-evento-produto');
});

function excluirEventoProduto(eventoId, produtoId){

    Swal.fire({
        title: 'Atenção',
        text: "Tem certeza que deseja excluir este produto? Esta é uma ação sem retorno.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText:  'Sim',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            loading('show');
            $.ajax({
                method: 'POST',
                url: '/painel/eventos/'+eventoId+'/produtos/excluir-evento-produto',
                data: { id:produtoId },
                success: function (data) {
                    loading('hidden');
                    
                    if(data.sucesso){
                        Swal.fire('', data.mensagem, 'success').then(() => {
                            document.location.reload(true);
                        });
                    }else if(data.validacao){
                        Swal.fire("Atenção!", data.mensagem, 'warning');
                    }else{
                        if(typeof data.interno !== 'undefined' && data.interno){
                            Swal.fire("", data.mensagem)
                        }else{
                            erroPadrao();
                        }
                    }
                },
                error: function () {
                    erroPadrao();
                },
                dataType:'json',
                async:false
            });
        }
    })
}
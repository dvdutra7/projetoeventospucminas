$(document).on('submit', '#form-evento', function (e) {
    e.preventDefault();
    formPadrao('#form-evento');
});

function ativarInativarEvento(id, ativo){

    if(ativo == 1){
        texto = 'inativar';
    }else{
        texto = 'ativar';
    }

    Swal.fire({
        title: 'Atenção',
        text: "Tem certeza que deseja "+texto+" este evento?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText:  'Sim',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            loading('show');
            $.ajax({
                method: 'POST',
                url: '/painel/eventos/ativar-inativar-evento',
                data: { id },
                success: function (data) {
                    loading('hidden');
                    
                    if(data.sucesso){
                        Swal.fire('', data.mensagem, 'success').then(() => {
                            document.location.reload(true);
                        });
                    }else if(data.validacao){
                        Swal.fire("Atenção!", data.mensagem, 'warning');
                    }else{
                        if(typeof data.interno !== 'undefined' && data.interno){
                            Swal.fire("", data.mensagem)
                        }else{
                            erroPadrao();
                        }
                    }
                },
                error: function () {
                    erroPadrao();
                },
                dataType:'json',
                async:false
            });
        }
    })
}
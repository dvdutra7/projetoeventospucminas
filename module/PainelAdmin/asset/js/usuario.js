$(document).on('submit', '#form-usuario', function (e) {
    e.preventDefault();
    formPadrao('#form-usuario');
});

$(document).on('submit', '#alterar-senha', function(){

    var dados = $(this).serialize();
    var action = $(this).attr('action');

    if($(this).valid()){
        loading('show');
        $.ajax({
            type: "POST",
            url: action,
            data: {dados: dados},
            success: function(data){
                loading('hidden');
                if(data.salvou){
                    Swal.fire(
                        '',
                        'Nova senha definida com sucesso.',
                        'success'
                    ).then(() => {
                        document.getElementById('#alterar-senha').reset();
                    });
                }else{
                    Swal.fire(
                        '',
                        data.mensagem,
                        'warning'
                    )
                }
            },
            error: function(data){
                loading('hidden');
                Swal.fire(
                    '',
                    'Falha ao processar a requisição',
                    'error'
                )
            },
            dataType:'json',
            async:false
        });
    }

    return false;
});

function ativarInativarUsuario(id, ativo){

    if(ativo == 1){
        texto = 'inativar';
    }else{
        texto = 'ativar';
    }

    Swal.fire({
        title: 'Atenção',
        text: "Tem certeza que deseja "+texto+" este usuário?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText:  'Sim',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            loading('show');
            $.ajax({
                method: 'POST',
                url: '/painel/usuarios/ativar-inativar-usuario',
                data: { id },
                success: function (data) {
                    loading('hidden');
                    
                    if(data.sucesso){
                        Swal.fire('', data.mensagem, 'success').then(() => {
                            document.location.reload(true);
                        });
                    }else if(data.validacao){
                        Swal.fire("Atenção!", data.mensagem, 'warning');
                    }else{
                        if(typeof data.interno !== 'undefined' && data.interno){
                            Swal.fire("", data.mensagem)
                        }else{
                            erroPadrao();
                        }
                    }
                },
                error: function () {
                    erroPadrao();
                },
                dataType:'json',
                async:false
            });
        }
    })
}

function modalRelacionarEventos(usuarioId){
    loading('show');

    status = $('#modal-usuario-evento select[name=status]').find(":selected").val() ? $('#modal-usuario-evento select[name=status]').find(":selected").val() : '';
    pesquisar = $('#modal-usuario-evento input[name=pesquisar]').val() ? $('#modal-usuario-evento input[name=pesquisar]').val() : '';

    $("#modal-usuario-evento").load('/painel/usuarios/modal-usuario-evento', {usuario_id:usuarioId, pesquisar, status}, function (responseText, textStatus) {
        if(textStatus != 'error'){
            $("#modal-usuario-evento").modal('show');
            loading('hidden');
            checks(usuarioId);
        }else{
            loading('hidden');
            erroPadrao();
        }  
    });
}

function checks(usuarioId){
    $('#modal-usuario-evento input[type=checkbox]').change(function() {
        eventoId = $(this).val();

        if($('input[name=check-evento-'+eventoId+']').is(':checked')){
            texto = 'vincular';
        }else{
            texto = 'desvincular';
        }

        Swal.fire({
            title: 'Atenção',
            text: "Tem certeza que deseja "+texto+" este evento com o usuário?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText:  'Sim',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                loading('show');
                $.ajax({
                    method: 'POST',
                    url: '/painel/usuarios/vincular-desvincular-usuario-evento',
                    data: { usuario_id:usuarioId, evento_id:eventoId },
                    success: function (data) {
                        loading('hidden');
                        
                        if(data.sucesso){
                            Swal.fire('', data.mensagem, 'success').then(() => {
                                
                            });
                        }else if(data.validacao){
                            Swal.fire("Atenção!", data.mensagem, 'warning');
                        }else{
                            if(typeof data.interno !== 'undefined' && data.interno){
                                Swal.fire("", data.mensagem)
                            }else{
                                erroPadrao();
                            }
                        }
                    },
                    error: function () {
                        erroPadrao();
                    },
                    dataType:'json',
                    async:false
                });
            }else{
                // Volta checkbox como antes
                if($('input[name=check-evento-'+eventoId+']').is(':checked')){
                    $('input[name=check-evento-'+eventoId+']').prop('checked', false);
                }else{
                    $('input[name=check-evento-'+eventoId+']').prop('checked', true);
                }
            }
        })
    });
}

function relacionarEvento(usuarioId, eventoId, relacionado){

    if(relacionado == 1){
        texto = 'desvincular';
    }else{
        texto = 'vincular';
    }

    Swal.fire({
        title: 'Atenção',
        text: "Tem certeza que deseja "+texto+" este evento com o usuário?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText:  'Sim',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            loading('show');
            $.ajax({
                method: 'POST',
                url: '/painel/eventos/ativar-inativar-evento',
                data: { id },
                success: function (data) {
                    loading('hidden');
                    
                    if(data.sucesso){
                        Swal.fire('', data.mensagem, 'success').then(() => {
                            document.location.reload(true);
                        });
                    }else if(data.validacao){
                        Swal.fire("Atenção!", data.mensagem, 'warning');
                    }else{
                        if(typeof data.interno !== 'undefined' && data.interno){
                            Swal.fire("", data.mensagem)
                        }else{
                            erroPadrao();
                        }
                    }
                },
                error: function () {
                    erroPadrao();
                },
                dataType:'json',
                async:false
            });
        }else{
            // Volta checkbox como antes
            if($('#checkbox-publicar-curso-'+curso_id).is(':checked')){
                $('#checkbox-publicar-curso-'+curso_id).prop('checked', false);
            }else{
                $('#checkbox-publicar-curso-'+curso_id).prop('checked', true);
            }
        }
    })
}
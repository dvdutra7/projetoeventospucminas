$("#data_inicio").datepicker({
    format: 'dd/mm/yyyy',
});

$("#data_fim").datepicker({
    format: 'dd/mm/yyyy',
});

function modalDetalhesRelatorioPdv(produtoId){
    loading('show');

    $("#modal-detalhes").load('/painel/relatorio-pdv/modal-detalhes', {produto_id:produtoId}, function (responseText, textStatus) {
        if(textStatus != 'error'){
            $("#modal-detalhes").modal('show');
            loading('hidden');
        }else{
            loading('hidden');
            erroPadrao();
        }  
    });
}
function carregaSelectProdutos(){
    var evento = $('select[name=venda_evento]').find(":selected").val();

    $(".lista-produtos").load('/painel/carrega-select-produtos', {evento:evento, preco:true}, function () {
    });
}

$(document).on('submit', '#form-ticket', function (e) {
    e.preventDefault();
    formPadrao('#form-ticket');
});

$("#data_inicio").datepicker({
    format: 'dd/mm/yyyy',
});

$("#data_fim").datepicker({
    format: 'dd/mm/yyyy',
});

function modalDetalhesRelatorio(usuarioId, produtoId){
    loading('show');

    $("#modal-detalhes").load('/painel/relatorio-vendas/modal-detalhes', {usuario_id:usuarioId, produto_id:produtoId}, function (responseText, textStatus) {
        if(textStatus != 'error'){
            $("#modal-detalhes").modal('show');
            loading('hidden');
        }else{
            loading('hidden');
            erroPadrao();
        }  
    });
}
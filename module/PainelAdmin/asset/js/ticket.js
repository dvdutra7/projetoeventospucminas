function liberarEntrada(ticket_id){
    
    loading('show');
    jQuery.ajax({
        type:'POST',
        url:'/ticket/liberar-entrada/'+ticket_id,
        data: {ticket_id: ticket_id},
        success: function(data){
            loading('hidden');
            Swal.fire({
              text: 'Entrada Liberada com sucesso!',
              icon: 'success',
              confirmButtonText: 'Ok',
              confirmButtonColor: '#3085d6',
            }).then((result) => {
                loading('show');
                window.location.href = '/painel/leitor-qr';
            })
        },
        error: function(jqXHR, textStatus, errorThrown){
            loading('hidden');
            erroPadrao();
        },
        dataType:'json',
        async:false
    });
};

function detalhesTicket(ticket_id){
    loading('show');

    $("#modal").load('/painel/ticket/modal-detalhes/'+ticket_id, {ticket_id:ticket_id}, function (responseText, textStatus) {
        if(textStatus != 'error'){
            $('.cpf-mask').mask('000.000.000-00');
            $("#modal").modal('show');
            loading('hidden');
        }else{
            loading('hidden');
            erroPadrao();
        }  
    });
}

function carregaSelectProdutos(){
    var evento = $('select[name=venda_evento]').find(":selected").val();

    $(".lista-produtos").load('/painel/carrega-select-produtos', {evento:evento, preco:false}, function () {
    });
}

function cancelarTicket(id){
    Swal.fire({
        title: 'ATENÇÃO!!',
        text: "Tem certeza que deseja cancelar o ticket deste cliente? Esta ação não pode ser desfeita.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'CANCELAR TICKET',
        cancelButtonText: 'DESISTIR'
    }).then((result) => {
        if (result.isConfirmed) {
            loading('show');
            $.ajax({
                method: 'POST',
                url: '/painel/ticket/cancelar-ticket',
                data: { id },
                success: function (data) {
                    loading('hidden');
                    data = JSON.parse(data);
                    Swal.fire(
                        '',
                        data.mensagem,
                        data.alertType
                    ).then((result) => {
                        if (result.isConfirmed) {
                            detalhesTicket(id);
                        }
                    });
                },
                error: function () {
                    loading('hidden');
                    erroPadrao();
                }
            });
        }
    })
}

function reenviarEmailTicket(id){
    Swal.fire({
        title: 'Atenção',
        text: "Ao confirmar, será enviado um novo e-mail com o QR Code de acesso para o cliente.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Reenviar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            loading('show');
            $.ajax({
                method: 'POST',
                url: '/painel/ticket/reenviar-email',
                data: { id },
                success: function (data) {
                    loading('hidden');
                    data = JSON.parse(data);
                    Swal.fire(
                        '',
                        data.mensagem,
                        data.type
                    )
                },
                error: function () {
                    loading('hidden');
                    erroPadrao();
                }
            });
        }
    })
}
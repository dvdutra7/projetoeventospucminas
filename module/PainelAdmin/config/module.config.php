<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace PainelAdmin;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\PainelController::class => InvokableFactory::class,
            Controller\UsuarioController::class => InvokableFactory::class,
            Controller\EventoController::class => InvokableFactory::class,
            Controller\EventoProdutoController::class => InvokableFactory::class,
            Controller\LeitorQrController::class => InvokableFactory::class,
            Controller\ClienteController::class => InvokableFactory::class,
            Controller\EventoPdvController::class => InvokableFactory::class,
            Controller\EventoPdvProdutoController::class => InvokableFactory::class,
            Controller\VendaController::class => InvokableFactory::class,
            Controller\TicketController::class => InvokableFactory::class,
            Controller\RelatorioController::class => InvokableFactory::class,
            Controller\RelatorioPdvController::class => InvokableFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'login' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => Controller\PainelController::class,
                        'action'     => 'login',
                    ],
                ],
            ],
            'logout' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\PainelController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],
            'recuperar-senha' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/recuperar-senha',
                    'defaults' => [
                        'controller' => Controller\PainelController::class,
                        'action'     => 'recuperar-senha',
                    ],
                ],
            ],
            'painel' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/painel[/:action]',
                    'defaults' => [
                        'controller' => Controller\PainelController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'usuarios' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/painel/usuarios[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z0-9-]*',
                        'id' => '[a-zA-Z0-9-]*',
                    ),
                    'defaults' => [
                        'controller' => Controller\UsuarioController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'alterar-senha' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/painel/alterar-senha',
                    'defaults' => [
                        'controller' => Controller\UsuarioController::class,
                        'action'     => 'alterar-senha',
                    ],
                ],
            ],
            'eventos' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/painel/eventos[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z0-9-]*',
                        'id' => '[a-zA-Z0-9-]*',
                    ),
                    'defaults' => [
                        'controller' => Controller\EventoController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'evento-produto' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/painel/eventos/:evento_id/produtos[/:action][/:id]',
                    'constraints' => array(
                        'evento_id' => '[a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => [
                        'controller' => Controller\EventoProdutoController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'eventos-pdv' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/painel/eventos-pdv[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z0-9-]*',
                        'id' => '[a-zA-Z0-9-]*',
                    ),
                    'defaults' => [
                        'controller' => Controller\EventoPdvController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'eventos-pdv-produto' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/painel/eventos-pdv/:evento_id/produtos[/:action][/:id]',
                    'constraints' => array(
                        'evento_id' => '[a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => [
                        'controller' => Controller\EventoPdvProdutoController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'leitor-qr' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/painel/leitor-qr[/:action]',
                    'defaults' => [
                        'controller' => Controller\LeitorQrController::class,
                        'action'     => 'index',
                    ]
                ]
            ],
            'clientes' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/painel/clientes[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => [
                        'controller' => Controller\ClienteController::class,
                        'action'     => 'index',
                    ]
                ]
            ],
            'vendas' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/painel/vendas[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => [
                        'controller' => Controller\VendaController::class,
                        'action'     => 'index',
                    ]
                ]
            ],
            'ticket' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/ticket[/:action][/:id]',
                    'constraints' => [
                        'id' => '[a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\TicketController::class,
                        'action' => 'index'
                    ],
                ],
            ],
            'ticket-painel' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/painel/ticket[/:action][/:id]',
                    'constraints' => [
                        'id' => '[a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\TicketController::class,
                        'action' => 'index'
                    ],
                ],
            ],
            'relatorio-vendas' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/painel/relatorio-vendas[/:action][/:id]',
                    'constraints' => [
                        'id' => '[a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\RelatorioController::class,
                        'action' => 'index'
                    ],
                ],
            ],
            'relatorio-pdv' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/painel/relatorio-pdv[/:action][/:id]',
                    'constraints' => [
                        'id' => '[a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\RelatorioPdvController::class,
                        'action' => 'index'
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/painel'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/no-layout'           => __DIR__ . '/../view/layout/no-layout.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'session_containers' => [
        'Session'
    ],
    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [__DIR__ . '/../asset'],
        ],
    ],
];
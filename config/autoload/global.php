<?php
use Zend\Session\Storage\SessionArrayStorage; 

return [
    'db' => array(
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=qrpass;host=localhost',//intra
        //'dsn'            => 'mysql:dbname=u469507726_qrpass;host=localhost',//hostinger
        'driver_options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ]
     ),

    'session_storage' => [
        'type' => SessionArrayStorage::class
    ],
    'session_config' => [
        // Session cookie will expire in 1 hour.
        'cookie_lifetime' => 60*60*1,     
        // Session data will be stored on server maximum for 30 days.
        'gc_maxlifetime'     => 60*60*24*30, 
    ],
];
